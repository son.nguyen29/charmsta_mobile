import { ImageSourcePropType } from "react-native/types"

export interface ImageSource {
  imageSource: ImageSourcePropType
}

export const AppIcon: ImageSource = {
  imageSource: require("../images/sad-face.png"),
}
