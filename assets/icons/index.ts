import { ImageSourcePropType } from "react-native/types"

export interface ImageSource {
  imageSource: ImageSourcePropType
}

export const Logo: ImageSource = {
  imageSource: require("../icons/slack.png"),
}
