export const REGEXP = {
  password: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,12}$/,
  phoneNumber: /^[+]?[(]?[0-9]{3}[)]?[-s.]?[0-9]{3}[-s.]?[0-9]{4,6}$/g,
}

export const PLACE_AUTOCOMPLETE =
  "https://maps.googleapis.com/maps/api/place/autocomplete/json?language=en&region=us"

export const PLACE_DETAILS =
  "https://maps.googleapis.com/maps/api/place/details/json"

export const STORE_CATEGORIES = Array.from({ length: 20 }, (_, i) => ({
  id: i,
  name: "Name " + i,
}))

export const STAFF_PERMISSION = [
  { id: 1, name: "Staff" },
  { id: 2, name: "Manager" },
]
