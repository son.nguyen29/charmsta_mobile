import { AddStaff } from "@models/request/Staff"
import {
  AUTH_SCREENS,
  COMMON_SCREENS,
  MAIN_SCREENS,
} from "@screens/screensName"

export type CommonNavigatorParamList = {
  [COMMON_SCREENS.welcome]: undefined
}

export type AuthNavigatorParamList = {
  [AUTH_SCREENS.login]: undefined
  [AUTH_SCREENS.register]: undefined
  [AUTH_SCREENS.forgot_password]: undefined
  [AUTH_SCREENS.store_form]: undefined
  [AUTH_SCREENS.search_store]: undefined
}

export type MainNavigatorParamList = {
  [MAIN_SCREENS.tabNavigator]: undefined
  [MAIN_SCREENS.home]: undefined
  [MAIN_SCREENS.customers]: undefined
  [MAIN_SCREENS.report]: undefined
  [MAIN_SCREENS.notifications]: undefined
  [MAIN_SCREENS.settings]: undefined
  [MAIN_SCREENS.profile]: undefined
  [MAIN_SCREENS.add_customer]: undefined
  [MAIN_SCREENS.import_customer]: undefined
  [MAIN_SCREENS.add_staff]: undefined
  [MAIN_SCREENS.categories]: undefined
  [MAIN_SCREENS.staff_service]: { staff: AddStaff }
  [MAIN_SCREENS.staff]: undefined
}
