import { createNativeStackNavigator } from "@react-navigation/native-stack"
import type { NativeStackNavigationProp } from "@react-navigation/native-stack"

import { AUTH_SCREENS } from "@screens/screensName"
import { AuthNavigatorParamList } from "@navigators/paramList"
import {
  LoginScreen,
  RegisterScreen,
  ForgotPasswordScreen,
  StoreFormScreen,
  SearchStoreScreen,
} from "@screens/auth"

export type AuthNavigationProp =
  NativeStackNavigationProp<AuthNavigatorParamList>

const Stack = createNativeStackNavigator<AuthNavigatorParamList>()

const AuthNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={AUTH_SCREENS.login}
    >
      <Stack.Screen name={AUTH_SCREENS.login} component={LoginScreen} />
      <Stack.Screen name={AUTH_SCREENS.register} component={RegisterScreen} />
      <Stack.Screen
        name={AUTH_SCREENS.forgot_password}
        component={ForgotPasswordScreen}
      />
      <Stack.Screen
        name={AUTH_SCREENS.store_form}
        component={StoreFormScreen}
      />
      <Stack.Screen
        name={AUTH_SCREENS.search_store}
        component={SearchStoreScreen}
      />
    </Stack.Navigator>
  )
}

export default AuthNavigator
