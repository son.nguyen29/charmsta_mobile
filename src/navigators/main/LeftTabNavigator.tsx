import { View, Pressable, StyleProp, ViewStyle, StyleSheet } from "react-native"
import {
  createNavigatorFactory,
  DefaultNavigatorOptions,
  ParamListBase,
  CommonActions,
  TabActionHelpers,
  TabNavigationState,
  TabRouter,
  TabRouterOptions,
  useNavigationBuilder,
} from "@react-navigation/native"
import { BottomTabNavigationOptions } from "@react-navigation/bottom-tabs"
import { SafeAreaProvider } from "react-native-safe-area-context"

import { Text } from "@components/basic/Text"

type TabNavigationConfig = {
  tabBarStyle?: StyleProp<ViewStyle>
  contentStyle?: StyleProp<ViewStyle>
}

type TabNavigationEventMap = {
  tabPress: {
    data: { isAlreadyFocused: boolean }
    canPreventDefault: true
  }
}

type Props = DefaultNavigatorOptions<
  ParamListBase,
  TabNavigationState<ParamListBase>,
  BottomTabNavigationOptions,
  TabNavigationEventMap
> &
  TabRouterOptions &
  TabNavigationConfig

export const LeftTabNavigator = ({
  initialRouteName,
  children,
  screenOptions,
  tabBarStyle,
  contentStyle,
}: Props) => {
  const { state, navigation, descriptors } = useNavigationBuilder<
    TabNavigationState<ParamListBase>,
    TabRouterOptions,
    TabActionHelpers<ParamListBase>,
    BottomTabNavigationOptions,
    TabNavigationEventMap
  >(TabRouter, {
    children,
    screenOptions,
    initialRouteName,
  })
  return (
    <SafeAreaProvider style={styles.container}>
      <View style={[styles.tabBar, tabBarStyle]}>
        {state.routes.map((route) => {
          const focused = route.key === state.routes[state.index].key
          const { title, tabBarShowLabel, tabBarIcon } =
            descriptors[route.key].options
          return (
            <Pressable
              key={route.key}
              onPress={() => {
                const event = navigation.emit({
                  type: "tabPress",
                  target: route.key,
                  canPreventDefault: true,
                  data: {
                    isAlreadyFocused: focused,
                  },
                })
                if (!event.defaultPrevented) {
                  navigation.dispatch({
                    ...CommonActions.navigate(route),
                    target: state.key,
                  })
                }
              }}
              style={styles.btn}
            >
              {tabBarIcon &&
                tabBarIcon?.({
                  focused,
                  color: "#000", // @todo
                  size: 20, // @todo
                })}
              {tabBarShowLabel && <Text>{title || route.name}</Text>}
            </Pressable>
          )
        })}
      </View>
      <View style={[styles.content, contentStyle]}>
        {state.routes.map((route, index) => {
          return (
            <View
              key={route.key}
              style={[
                StyleSheet.absoluteFill,
                index === state.index ? styles.showScreen : styles.hideScreen,
              ]}
            >
              {descriptors[route.key].render()}
            </View>
          )
        })}
      </View>
    </SafeAreaProvider>
  )
}

export const createLeftTabNavigator = createNavigatorFactory<
  TabNavigationState<ParamListBase>,
  BottomTabNavigationOptions,
  TabNavigationEventMap,
  typeof LeftTabNavigator
>(LeftTabNavigator)

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },
  tabBar: {
    height: "100%",
    overflow: "hidden",
    flexDirection: "column",
    justifyContent: "space-between",
    backgroundColor: "#F2F2F2",
    width: 93,
    padding: 8,
    borderRightWidth: 1,
    borderRightColor: "#D8D8D8",
  },
  content: {
    flex: 1,
  },
  showScreen: {
    display: "flex",
  },
  hideScreen: {
    display: "none",
  },
  btn: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 4,
  },
})
