import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { RFValue } from "d4dpocket"
import { isTablet } from "react-native-device-info"
import AntDesign from "react-native-vector-icons/AntDesign"

import { MainNavigatorParamList } from "@navigators/paramList"
import {
  AddServiceScreen,
  CustomersScreen,
  HomeScreen,
  ProfileScreen,
} from "@screens/main"
import { CategoriesScreen } from "@screens/main/category"
import { MAIN_SCREENS } from "@screens/screensName"
import { createLeftTabNavigator } from "./LeftTabNavigator"

const Tab = isTablet()
  ? createLeftTabNavigator<MainNavigatorParamList>()
  : createBottomTabNavigator<MainNavigatorParamList>()

type IconProps = {
  name: string
  focused?: boolean
}

const Icon = ({ name, focused }: IconProps) => (
  <AntDesign
    name={name}
    size={RFValue(20)}
    color={focused ? "#ff0000" : "#000000"}
  />
)

const TabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName={MAIN_SCREENS.home}
      screenOptions={{ headerShown: false, tabBarShowLabel: false }}
    >
      <Tab.Screen
        name={MAIN_SCREENS.home}
        options={{
          tabBarIcon: ({ focused }) => (
            <Icon name="calendar" {...{ focused }} />
          ),
        }}
        component={HomeScreen}
      />
      <Tab.Screen
        name={MAIN_SCREENS.customers}
        options={{
          tabBarIcon: ({ focused }) => <Icon name="user" {...{ focused }} />,
        }}
        component={CategoriesScreen || CustomersScreen}
      />
      <Tab.Screen
        name={MAIN_SCREENS.report}
        options={{
          tabBarIcon: ({ focused }) => (
            <Icon name="linechart" {...{ focused }} />
          ),
        }}
        component={ProfileScreen}
      />
      <Tab.Screen
        name={MAIN_SCREENS.notifications}
        options={{
          tabBarIcon: ({ focused }) => <Icon name="bells" {...{ focused }} />,
        }}
        component={ProfileScreen}
      />
      <Tab.Screen
        name={MAIN_SCREENS.settings}
        options={{
          tabBarIcon: ({ focused }) => <Icon name="setting" {...{ focused }} />,
        }}
        component={AddServiceScreen}
      />
    </Tab.Navigator>
  )
}

export default TabNavigator
