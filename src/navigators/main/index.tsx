import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { StackNavigationProp } from "@react-navigation/stack"

import { MainNavigatorParamList } from "@navigators/paramList"
import {
  AddCustomerScreen,
  AddStaffScreen,
  StaffServiceScreen,
  StaffScreen,
} from "@screens/main"
import { CategoriesScreen } from "@screens/main/category"
import { ImportCustomerScreen } from "@screens/main/customer"
import { MAIN_SCREENS } from "@screens/screensName"
import TabNavigator from "./TabNavigator"

export type MainNavigationProp = StackNavigationProp<MainNavigatorParamList>

const Stack = createNativeStackNavigator<MainNavigatorParamList>()

const MainNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={MAIN_SCREENS.home}
    >
      <Stack.Screen name={MAIN_SCREENS.tabNavigator} component={TabNavigator} />
      <Stack.Screen
        name={MAIN_SCREENS.add_customer}
        component={AddCustomerScreen}
      />
      <Stack.Screen
        name={MAIN_SCREENS.import_customer}
        component={ImportCustomerScreen}
      />
      <Stack.Screen name={MAIN_SCREENS.add_staff} component={AddStaffScreen} />
      <Stack.Screen
        name={MAIN_SCREENS.staff_service}
        component={StaffServiceScreen}
      />
      <Stack.Screen
        name={MAIN_SCREENS.categories}
        component={CategoriesScreen}
      />
      <Stack.Screen name={MAIN_SCREENS.staff} component={StaffScreen} />
    </Stack.Navigator>
  )
}

export default MainNavigator
