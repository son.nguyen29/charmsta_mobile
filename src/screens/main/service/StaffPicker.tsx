import React from "react"
import { Text } from "react-native"

import { MultiSelect } from "@components/index"
import { usStaffService } from "@hooks/staff/usStaffService"
import { PaginationStaffDTO } from "@models/response/Staff"

const StaffPicker = () => {
  const { data } = usStaffService({})
  return (
    <MultiSelect<PaginationStaffDTO> data={data}>
      <Text>Staffs</Text>
    </MultiSelect>
  )
}

export default StaffPicker
