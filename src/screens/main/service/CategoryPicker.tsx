import React from "react"
import { Text } from "react-native"

import { Picker } from "@components/index"
import { useCategory } from "@hooks/category/useCategory"

const CategoryPicker = () => {
  const { data } = useCategory({})
  return (
    <Picker data={data}>
      <Text>Category</Text>
    </Picker>
  )
}

export default CategoryPicker
