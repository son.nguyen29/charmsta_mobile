import { string, object, number, array, InferType, ObjectSchema } from "yup"

import { AddService } from "@models/request/Service"

const ServiceSchema: ObjectSchema<AddService> = object({
  name: string()
    .trim("Name is required.")
    .min(2, "Too short.")
    .required("Name is required."),
  serviceDuration: string()
    .trim("Duration is required.")
    .required("Duration is required."),
  cost: string().trim("Cost is required.").required("Cost is required."),
  category: number().required("Category is required."),
  staff: array().min(1, "Staff is required.").required("Staff is required."),
})

export type IService = InferType<typeof ServiceSchema>
export default ServiceSchema
