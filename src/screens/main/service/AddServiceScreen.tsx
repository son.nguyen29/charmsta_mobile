import React from "react"
import { View, Text, Button } from "react-native"
import { useFormik } from "formik"

import ServiceSchema, { IService } from "./ServiceSchema"
import CategoryPicker from "./CategoryPicker"
import StaffPicker from "./StaffPicker"
import { Field } from "@components/index"
import { useCreateService } from "@hooks/service/useCreateService"

const initialValues: IService = {
  name: "",
  serviceDuration: "",
  cost: "",
  category: 0,
  staff: [],
}

const initialErrors = {
  name: "",
  serviceDuration: "",
  cost: "",
  category: "",
  staff: "",
}

const AddServiceScreen = () => {
  const {
    isValid,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
  } = useFormik({
    initialValues,
    initialErrors,
    validationSchema: ServiceSchema,
    onSubmit: () => onSubmit(),
  })

  const { isLoading, runRequest } = useCreateService({})

  const onSubmit = () => {
    runRequest(values)
  }
  return (
    <View>
      <Text>AddServiceScreen</Text>
      <CategoryPicker />
      <Field
        required
        name="name"
        label="Name"
        placeholder="Enter your name"
        value={values.name}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Field
        required
        name="duration"
        label="Duration"
        placeholder="Enter your duration"
        value={values.serviceDuration}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Field
        required
        name="cost"
        label="Cost"
        placeholder="Enter your cost"
        value={values.cost}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <StaffPicker />
      <Button
        disabled={!isValid || isLoading}
        title="Submit"
        onPress={() => handleSubmit()}
      />
    </View>
  )
}

export default AddServiceScreen
