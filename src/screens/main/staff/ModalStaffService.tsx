import React, { useState } from "react"
import { View, Text, StyleSheet, FlatList } from "react-native"
import Modal from "react-native-modal"
import { RFValue, Button, scale } from "d4dpocket"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import MCIcons from "react-native-vector-icons/MaterialCommunityIcons"

import { SearchBar } from "@components/index"

const data = Array.from({ length: 20 }, (_, i) => ({
  id: i,
  name: "Service " + i,
}))

const ModalStaffService = () => {
  const [isVisible, setVisible] = useState<boolean>(false)

  const openModal = () => {
    setVisible(true)
  }

  const closeModal = () => {
    setVisible(false)
  }

  const renderItem = ({ item }) => {
    const isSelected = false
    return (
      <View style={styles.item}>
        <Text>{item.name}</Text>
        <View style={styles.row}>
          <FontAwesome name="clock-o" size={RFValue(18)} color="#000" />
          <Text>45m</Text>
        </View>
        <View style={styles.row}>
          <FontAwesome name="dollar" size={RFValue(18)} color="#000" />
          <Text>50$</Text>
        </View>
        <Button>
          <MCIcons
            color="#000"
            size={RFValue(18)}
            name={isSelected ? "checkbox-marked" : "checkbox-blank-outline"}
          />
        </Button>
      </View>
    )
  }

  const keyExtractor = (_, index) => index.toString()

  return (
    <View>
      <Button onPress={openModal}>
        <Text>List service</Text>
      </Button>
      <Modal
        useNativeDriver
        isVisible={isVisible}
        backdropTransitionOutTiming={0}
        hideModalContentWhileAnimating
        onBackButtonPress={closeModal}
        onBackdropPress={closeModal}
        style={styles.modal}
      >
        <View style={styles.modalContainer}>
          <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={keyExtractor}
            style={styles.content}
            showsVerticalScrollIndicator={false}
            stickyHeaderIndices={[0]}
            ListHeaderComponent={<SearchBar />}
          />
        </View>
      </Modal>
    </View>
  )
}

const styles = StyleSheet.create({
  modal: {
    marginVertical: scale(10),
    marginHorizontal: scale(20),
    justifyContent: "flex-end",
  },
  modalContainer: {
    maxHeight: "90%",
  },
  content: {
    backgroundColor: "#fff",
    borderRadius: scale(12),
  },
  item: {
    paddingVertical: scale(6),
    paddingHorizontal: scale(12),
    borderTopColor: "#F2F2F2",
    borderTopWidth: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
})

export default ModalStaffService
