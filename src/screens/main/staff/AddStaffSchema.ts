import { string, object, number, array, InferType, ObjectSchema } from "yup"

import { AddStaff } from "@models/request/Staff"

const AddStaffSchema: ObjectSchema<AddStaff> = object({
  avatar: string(),
  name: string()
    .trim("Name is required.")
    .min(2, "Too short.")
    .required("Name is required."),
  phoneNumber: string()
    .trim("Phone number is required.")
    .min(10, "Please enter a valid phone number.")
    .max(12, "Please enter a valid phone number.")
    .required("Phone number is required."),
  email: string()
    .trim("Email is required.")
    .email("Please enter a valid email.")
    .required("Email is required."),
  break: array().min(1, "Break is required.").required("Break is required."),
  workingDay: array()
    .min(1, "Working day is required.")
    .required("Working day is required."),
  permission: number().required("Permission is required."),
})

export type IAddStaff = InferType<typeof AddStaffSchema>
export default AddStaffSchema
