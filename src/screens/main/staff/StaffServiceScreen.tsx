import React, { useState } from "react"
import { View, Text, FlatList, StyleSheet, Alert } from "react-native"
import { RFValue, Button, scale } from "d4dpocket"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import MCIcons from "react-native-vector-icons/MaterialCommunityIcons"
import { useRoute } from "@react-navigation/native"
import type { RouteProp } from "@react-navigation/native"

import ModalStaffService from "./ModalStaffService"
import { useCreateStaff } from "@hooks/staff/useCreateStaff"
import { MainNavigatorParamList } from "@navigators/paramList"
import { MAIN_SCREENS } from "@screens/screensName"

const data = Array.from({ length: 5 }, (_, i) => ({
  id: i,
  name: "Service " + i,
}))

const StaffServiceScreen = () => {
  const route =
    useRoute<RouteProp<MainNavigatorParamList, MAIN_SCREENS.staff_service>>()
  const [services, setServices] = useState(data)

  const successCallback = () => {
    Alert.alert("Add staff success")
  }

  const failureCallback = () => {
    Alert.alert("Error, please try again")
  }

  const { isLoading, runRequest } = useCreateStaff({
    successCallback,
    failureCallback,
  })

  const onSubmit = () => {
    const { staff } = route.params
    const data = {
      avatar: staff.avatar,
      name: staff.name,
      phoneNumber: staff.phoneNumber,
      email: staff.email,
      break: [{ id: 1 }],
      workingDay: [{ id: 1 }],
      permission: 1,
    }
    runRequest(data)
  }

  const removeItem = (serviceId) => {
    const newServices = services.filter((service) => service.id !== serviceId)
    setServices(newServices)
  }

  const renderItem = ({ item }) => {
    return (
      <View style={styles.item}>
        <Text>{item.name}</Text>
        <View style={styles.row}>
          <FontAwesome name="clock-o" size={RFValue(18)} color="#000" />
          <Text>45m</Text>
        </View>
        <View style={styles.row}>
          <FontAwesome name="dollar" size={RFValue(18)} color="#000" />
          <Text>50$</Text>
        </View>
        <Button onPress={() => removeItem(item.id)}>
          <MCIcons color="#000" size={RFValue(18)} name="close" />
        </Button>
      </View>
    )
  }

  const keyExtractor = (_, index) => index.toString()

  return (
    <View>
      <Text>StaffServiceScreen</Text>
      <ModalStaffService />
      <FlatList
        data={services}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        showsVerticalScrollIndicator={false}
      />
      <Button disabled={isLoading} onPress={onSubmit}>
        <Text>Save</Text>
      </Button>
    </View>
  )
}

const styles = StyleSheet.create({
  item: {
    paddingVertical: scale(6),
    paddingHorizontal: scale(12),
    borderTopColor: "#F2F2F2",
    borderTopWidth: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
})

export default StaffServiceScreen
