import React from "react"
import { View, FlatList, StyleSheet, ActivityIndicator } from "react-native"

import { Text, SearchBar } from "@components/index"
import { useStaff } from "@hooks/staff/useStaff"

const StaffScreen = () => {
  const { isLoading, data } = useStaff({})

  const keyExtractor = (_, index) => index.toString()

  const renderItem = ({ item }) => (
    <View style={styles.item}>
      <Text>{item.name}</Text>
      <Text>Active</Text>
    </View>
  )

  return (
    <View>
      <SearchBar />
      <FlatList
        data={data?.content}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        ListHeaderComponent={isLoading && <ActivityIndicator />}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  item: {
    flexDirection: "row",
    alignItems: "center",
  },
})

export default StaffScreen
