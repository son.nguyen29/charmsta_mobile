import React from "react"
import { View, Text, Button } from "react-native"
import { useFormik } from "formik"
import { useNavigation } from "@react-navigation/native"

import AddStaffSchema, { IAddStaff } from "./AddStaffSchema"
import { Field, ImagePicker } from "@components/index"
import Permission from "./Permission"
import { MainNavigationProp } from "@navigators/main"
import { MAIN_SCREENS } from "@screens/screensName"

const initialValues: IAddStaff = {
  avatar: "",
  name: "",
  phoneNumber: "",
  email: "",
  break: [{ id: 1 }],
  workingDay: [{ id: 1 }],
  permission: 1,
}

const initialErrors = {
  name: "",
  phoneNumber: "",
  email: "",
  break: "",
  workingDay: "",
}

const AddStaffScreen = () => {
  const navigation = useNavigation<MainNavigationProp>()
  const {
    isValid,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
  } = useFormik({
    initialValues,
    initialErrors,
    validationSchema: AddStaffSchema,
    onSubmit: () => onSubmit(),
  })

  const onSubmit = () => {
    navigation.navigate(MAIN_SCREENS.staff_service, { staff: values })
  }
  return (
    <View>
      <Text>AddServiceScreen</Text>
      <ImagePicker>
        <Text>Avatar</Text>
      </ImagePicker>
      <Field
        required
        name="name"
        label="Name"
        placeholder="Enter your name"
        value={values.name}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Field
        required
        name="phoneNumber"
        label="Phone number"
        placeholder="Enter your phone number"
        value={values.phoneNumber}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Field
        required
        name="email"
        label="Email"
        placeholder="Enter your email"
        value={values.email}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Permission
        name="permission"
        value={values.permission}
        {...{ setFieldValue }}
      />
      <Button
        disabled={!isValid}
        title="Submit"
        onPress={() => handleSubmit()}
      />
    </View>
  )
}

export default AddStaffScreen
