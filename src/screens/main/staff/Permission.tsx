import React from "react"
import { View, Text, StyleSheet } from "react-native"
import { Button, RFValue } from "d4dpocket"
import MaterialIcons from "react-native-vector-icons/MaterialIcons"

import { STAFF_PERMISSION } from "@config/constants"

const Permission = ({ name, value, setFieldValue }) => {
  return (
    <View>
      <Text>Permission:</Text>
      <View style={styles.content}>
        {STAFF_PERMISSION.map((permission) => {
          const isSelected = value === permission.id
          return (
            <Button
              key={permission.id}
              onPress={() => setFieldValue(name, permission.id)}
            >
              <MaterialIcons
                name={`radio-button-${isSelected ? "on" : "off"}`}
                size={RFValue(18)}
                color="#000"
              />
              <Text>{permission.name}</Text>
            </Button>
          )
        })}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  content: {
    flexDirection: "row",
    alignItems: "center",
  },
})

export default Permission
