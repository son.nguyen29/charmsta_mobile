import { colors } from "@theme/colors";
import { spacing } from "@theme/spacing";
import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  contactContainer: {
    alignItems: "center",
    borderColor: colors.palette.neutral100,
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: "row",
    // height: 50,
    justifyContent: "space-between",
    marginBottom: spacing.medium,
    marginHorizontal: spacing.medium,
    padding: spacing.medium,
  },
  contactList: {
    paddingVertical: spacing.medium,
  },
})