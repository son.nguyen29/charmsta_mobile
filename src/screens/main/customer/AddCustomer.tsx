import Avatar from "@components/Avatar"
import { DatePickerModal, DatePickerModalRef } from "@components/DatePicker"
import { Field } from "@components/Field"
import { ImagePicker } from "@components/ImagePicker"
import { useCreateCustomer } from "@hooks/customer"
import { useUploadImages } from "@hooks/utils/useUploadImages"
import { translate } from "@i18n/translate"
import { CreateCustomer } from "@models/request/Customer"
import { CustomerDTO } from "@models/response/Customer"
import { UploadDTO } from "@models/response/Utils"
import { globalStyles } from "@theme/globalStyle"
import { useFormik } from "formik"
import { isEmpty } from "lodash"
import React, { useEffect, useRef } from "react"
import { Alert, Button, ScrollView, Text, View } from "react-native"
import { Image } from "react-native-image-crop-picker"
import { AddCustomerSchema } from "./AddCustomerSchema"

interface AddCustomerProps {}

const initialValues: CreateCustomer = {
  avatar: "",
  city: "",
  dob: "",
  firstName: "",
  lastName: "",
  phoneNumber: "",
  state: "",
  zipcode: "",
  email: "",
  address: "",
}

const initialErrors = {
  avatar: "",
  city: "",
  dob: "",
  firstName: "",
  lastName: "",
  phoneNumber: "",
  state: "",
  zipcode: "",
  email: "",
  address: "",
}

const CustomersScreen = (props: AddCustomerProps) => {
  const {
    error: createCustomerError,
    isLoading,
    runRequest,
  } = useCreateCustomer({
    successCallback: (customer) => successCallback(customer),
  })
  const {
    error: uploadingImagesError,
    isLoading: imagesUploading,
    runRequest: uploadImages,
  } = useUploadImages({
    successCallback: (uploads) => uploadImagesSuccess(uploads),
  })
  const {
    isValid,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
  } = useFormik({
    initialValues,
    initialErrors,
    onSubmit: () => onSubmit(),
    validationSchema: AddCustomerSchema,
  })

  const datePickerRef = useRef<DatePickerModalRef>()

  useEffect(() => {
    if (!isEmpty(createCustomerError) || !isEmpty(uploadingImagesError)) {
      Alert.alert("Error", translate("errors.unexpected"))
    }
  }, [createCustomerError, uploadingImagesError])

  const onSubmit = () => {
    runRequest(values)
  }

  const successCallback = (customer: CustomerDTO) => {}

  const onSelectImage = (photo: Image) => {
    setFieldValue("avatar", photo.path)
    uploadImages([photo])
  }

  const uploadImagesSuccess = (
    uploads: Array<UploadDTO & { belongedToIndex: number }>,
  ) => {
    setFieldValue("avatar", uploads[0].url)
  }

  const handleOpenDatePicker = () => {
    if (datePickerRef?.current) {
      datePickerRef.current.setModalVisible(true)
    }
  }

  const handleDOBChange = (date: any) => {
    setFieldValue("dob", date)
  }

  return (
    <View style={globalStyles.container}>
      <Text>AddCustomer</Text>
      <ImagePicker onSelect={onSelectImage}>
        <Avatar uri={values.avatar} loading={imagesUploading} />
      </ImagePicker>
      <ScrollView>
        <Field
          name="firstName"
          label="First name"
          placeholder="Enter your first name"
          value={values.firstName}
          {...{ handleChange, handleBlur, errors, touched }}
        />
        <Field
          name="lastName"
          label="Last name"
          placeholder="Enter your last name"
          value={values.lastName}
          {...{ handleChange, handleBlur, errors, touched }}
        />
        <Field
          required
          name="phoneNumber"
          label="Phone number"
          placeholder="Enter your phone number"
          value={values.phoneNumber}
          {...{ handleChange, handleBlur, errors, touched }}
        />
        <Field
          name="email"
          label="Email"
          placeholder="Enter your email"
          value={values.email}
          {...{ handleChange, handleBlur, errors, touched }}
        />
        <Field
          name="dob"
          label="Date of birth"
          placeholder="Enter your date of birth"
          onPress={handleOpenDatePicker}
          value={values.dob}
          {...{ handleChange, handleBlur, errors, touched }}
        />
        <Field
          name="address"
          label="Address"
          placeholder="Enter your address"
          value={values.address}
          {...{ handleChange, handleBlur, errors, touched }}
        />
        <Field
          name="city"
          label="City"
          placeholder="Enter your city"
          value={values.city}
          {...{ handleChange, handleBlur, errors, touched }}
        />
        <Field
          name="state"
          label="State"
          placeholder="Enter your state"
          value={values.state}
          {...{ handleChange, handleBlur, errors, touched }}
        />
        <Field
          name="zipcode"
          label="Zipcode"
          placeholder="Enter your zipcode"
          value={values.zipcode}
          {...{ handleChange, handleBlur, errors, touched }}
        />
      </ScrollView>
      <Button
        disabled={!isValid || isLoading}
        title={isLoading ? "Create ..." : "Create"}
        onPress={() => handleSubmit()}
      />
      <DatePickerModal
        datePickerRef={datePickerRef}
        onDateChange={handleDOBChange}
      />
    </View>
  )
}

export default CustomersScreen
