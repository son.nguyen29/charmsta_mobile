import { Text } from "@components/index"
import { useImportCustomers } from "@hooks/customer"
import { useUploadImages } from "@hooks/utils"
import { useContactPermissions } from "@hooks/utils/useContactPermissions"
import { ImportCustomer } from "@models/request/Customer"
import { UploadDTO } from "@models/response/Utils"
import { colors } from "@theme/colors"
import { globalStyles } from "@theme/globalStyle"
import { isEmpty } from "lodash"
import moment from "moment"
import React, { useEffect, useState } from "react"
import {
  Button,
  FlatList,
  TextInput,
  TouchableOpacity,
  View
} from "react-native"
import RNContacts, { Contact } from "react-native-contacts"
import Ionicons from "react-native-vector-icons/Ionicons"
import { styles } from "./styles"
interface ImportCustomerScreenProps { }

const ImportCustomerScreen = (props: ImportCustomerScreenProps) => {
  const [contacts, setContacts] = useState<Contact[]>([])
  const [selecteds, setSelecteds] = useState<Contact[]>([])
  const { requestPermissions, isGranted } = useContactPermissions()
  const { runRequest } = useImportCustomers({})
  const { runRequest: uploadImages } = useUploadImages({
    successCallback: (
      uploads: Array<UploadDTO & { belongedToIndex: number }>,
    ) => onImportCustomers(uploads),
  })

  useEffect(() => {
    if (isGranted) {
      RNContacts.getAll().then((response) => {
        setContacts(response)
      })
    }
  }, [isGranted])

  useEffect(() => {
    if (!isGranted) {
      requestPermissions()
    }
  }, [])

  const onImportCustomers = (
    uploads: Array<UploadDTO & { belongedToIndex: number }>,
  ) => {
    if (!isEmpty(uploads)) {
      const formattedData: ImportCustomer[] = selecteds.map((selected) => {
        const {
          phoneNumbers,
          birthday,
          givenName,
          familyName,
          emailAddresses,
          postalAddresses,
          thumbnailPath,
          hasThumbnail,
        } = selected

        return {
          avatar: hasThumbnail ? thumbnailPath : null,
          phoneNumber: phoneNumbers.length > 0 ? phoneNumbers[0].number : null,
          dob: birthday ? moment().set(birthday).unix().toString() : null,
          firstName: givenName,
          lastName: familyName,
          email: emailAddresses.length > 0 ? emailAddresses[0].email : null,
          address: postalAddresses[0].street,
          city: postalAddresses[0].city,
          state: postalAddresses[0].state,
          zipcode: postalAddresses[0].postCode,
        }
      })
      uploads.forEach((image) => {
        if (!isEmpty(image)) {
          const { belongedToIndex, url } = image
          formattedData[belongedToIndex].avatar = url
        }
      })
      runRequest(formattedData)
    }
  }

  const onAddPress = () => {
    uploadImages(
      selecteds.map((item) => ({
        path: item.hasThumbnail ? item.thumbnailPath : null,
      })) as any,
    )
  }

  const RenderItem = ({ item, index }: { item: Contact; index: number }) => {
    const { phoneNumbers, givenName, familyName, recordID } = item

    const isSelected = selecteds.some(
      (selected) => selected.recordID === recordID,
    )

    const onItemPress = () => {
      if (isSelected) {
        setSelecteds(
          selecteds.filter(
            ({ recordID: selectedId }) => selectedId !== recordID,
          ),
        )
      } else {
        setSelecteds([...selecteds, item])
      }
    }

    return (
      <TouchableOpacity onPress={onItemPress} style={styles.contactContainer}>
        <View>
          <Text text={`${givenName} ${familyName}`} />
          {phoneNumbers[0] && <Text text={phoneNumbers[0]?.number} />}
        </View>
        {isSelected && (
          <Ionicons
            name="checkmark"
            size={24}
            color={colors.palette.primary300}
          />
        )}
      </TouchableOpacity>
    )
  }

  return (
    <View style={globalStyles.container}>
      <Text>ImportCustomerScreen</Text>
      <TextInput placeholder="search" />

      <FlatList
        data={contacts}
        keyExtractor={(item) => item.recordID}
        renderItem={({ item, index }) => (
          <RenderItem item={item} index={index} />
        )}
        style={styles.contactList}
      />
      <Button title="Add" onPress={onAddPress} />
    </View>
  )
}

export default ImportCustomerScreen


