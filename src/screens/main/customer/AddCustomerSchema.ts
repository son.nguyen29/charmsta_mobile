import { validateRegex } from "@utils/yup"
import * as yup from "yup"

export const AddCustomerSchema = yup.object().shape({
  phoneNumber: yup
    .string()
    .matches(validateRegex.phoneNumber, "Phone number is not correct")
    .required("Phone numer is required"),
  email: yup.string().email("Email format is not correct").nullable(),
})
