import Avatar from "@components/Avatar"
import FloatButton from "@components/FloatButton"
import SearchBar from "@components/SearchBar"
import { Text } from "@components/index"
import { useGetCustomers } from "@hooks/customer"
import { CustomerDTO } from "@models/response/Customer"
import { useNavigation } from "@react-navigation/native"
import { MAIN_SCREENS } from "@screens/screensName"
import { AxiosError } from "axios"
import React, { useEffect, useState } from "react"
import { FlatList, TouchableOpacity, View } from "react-native"

const RenderItem = ({ item }: { item: CustomerDTO }) => {
  return (
    <View style={{ flexDirection: "row" }}>
      <Avatar uri={item?.avatar} />
      <View>
        <Text text={`${item.firstName} ${item.lastName}`.trim()} />
        <Text text={`${item.phoneNumber}`.trim()} />
        {/* ${item.countryCode}  */}
      </View>
    </View>
  )
}

const CustomersScreen = () => {
  const navigation = useNavigation()

  const [showOption, setShowOption] = useState(false)
  const [customers, setCustomers] = useState<CustomerDTO[]>([])
  const [take, setTake] = useState(10)
  const { error, isLoading, runRequest } = useGetCustomers({
    successCallback: (data) => successCallback(data),
    failureCallback: (err) => failureCallback(err),
  })

  useEffect(() => {
    runRequest()
  }, [])

  const successCallback = (data: CustomerDTO[]) => {
    setCustomers(data)
  }
  const failureCallback = (err: AxiosError) => {
    //handle failure
  }

  const handleGetMore = () => {
    if (customers.length % take == 0) {
      setTake(take + 10)
      runRequest({ take: take + 10 })
    }
  }

  return (
    <View style={{ flex: 1 }}>
      <Text>CustomersScreen</Text>
      <SearchBar />
      <FlatList
        onEndReached={handleGetMore}
        ListEmptyComponent={() => (
          <View>
            <Text text="No Data" />
          </View>
        )}
        data={customers}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item, index }) => <RenderItem item={item} />}
      />
      <>
        {showOption && (
          <View style={{ position: "absolute", right: 10, bottom: 100 }}>
            {[
              { key: "addNew", screenName: MAIN_SCREENS.add_customer },
              {
                key: "importFromContacts",
                screenName: MAIN_SCREENS.import_customer,
              },
            ].map((item) => (
              <TouchableOpacity
                onPress={() => navigation.navigate(item.screenName as never)}
              >
                <Text text={item.key} />
              </TouchableOpacity>
            ))}
          </View>
        )}
        <FloatButton
          size={80}
          content={showOption ? "x" : "+"}
          onPress={() => setShowOption(!showOption)}
        />
      </>
    </View>
  )
}

export default CustomersScreen
