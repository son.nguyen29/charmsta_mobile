import { AddCustomerScreen, CustomersScreen } from "./customer"
import HomeScreen from "./home"
import ProfileScreen from "./profile"
import { AddServiceScreen } from "./service"
import { AddStaffScreen, StaffServiceScreen, StaffScreen } from "./staff"

export {
  HomeScreen,
  CustomersScreen,
  AddCustomerScreen,
  ProfileScreen,
  AddServiceScreen,
  AddStaffScreen,
  StaffServiceScreen,
  StaffScreen,
}
