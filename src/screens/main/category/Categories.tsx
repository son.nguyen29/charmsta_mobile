import { Text } from '@components/index';
import { useGetCategories } from '@hooks/category';
import { CategoryDTO } from '@models/response/Category';
import { globalStyles } from '@theme/globalStyle';
import { debounce } from 'lodash';
import React, { useEffect, useState } from 'react';
import { FlatList, TextInput, TouchableOpacity, View } from 'react-native';

interface CategoriesScreenProps { }

const CategoriesScreen = (props: CategoriesScreenProps) => {

  const [categories, setCategories] = useState<CategoryDTO[]>([])

  const { runRequest } = useGetCategories({ successCallback: (categories) => onGetCategoriesSuccess(categories) })

  useEffect(() => {
    runRequest()
  }, [])

  const onGetCategoriesSuccess = (categories: CategoryDTO[]) => {
    setCategories(categories)
  }

  const searchAction = debounce((text: string) => {
    runRequest({ search: text.trim() })
  }, 1000)

  const RenderItem = ({ item }: { item: CategoryDTO }) => {

    const onItemPress = () => {
      // handle category press
    }

    return <TouchableOpacity onPress={onItemPress}>
      <Text text={item.name} />
    </TouchableOpacity>
  }


  return (
    <View style={globalStyles.container}>
      <Text>CategoriesScreen</Text>
      <TextInput placeholder='Search' onChangeText={searchAction} />
      <FlatList
        data={categories}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => <RenderItem item={item} />}
      />
    </View>
  );
};

export default CategoriesScreen;


