import { string, object, array, InferType, ObjectSchema } from "yup"

import { AddCategory } from "@models/request/Category"

const AddCategorySchema: ObjectSchema<AddCategory> = object({
  name: string()
    .trim("Name is required.")
    .min(2, "Too short.")
    .required("Name is required."),
  services: array()
    .min(1, "Services is required.")
    .required("Services is required.")
})

export type IAddCategory = InferType<typeof AddCategorySchema>
export default AddCategorySchema
