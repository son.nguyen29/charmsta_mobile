import React from "react"
import { Text } from "react-native"

import { MultiSelect } from "@components/index"
import { useService } from "@hooks/service/useService"
import { ServiceDTO } from "@models/response/Service"

const ServicePicker = () => {
  const { data } = useService({})
  return (
    <MultiSelect<ServiceDTO> data={data}>
      <Text>Services</Text>
    </MultiSelect>
  )
}

export default ServicePicker
