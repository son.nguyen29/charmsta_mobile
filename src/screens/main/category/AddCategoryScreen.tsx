import React from "react"
import { View, Text, Button } from "react-native"
import { useFormik } from "formik"

import ServicePicker from "./ServicePicker"
import AddCategorySchema, { IAddCategory } from "./AddCategorySchema"
import { Field } from "@components/index"
import { useCreateCategory } from "@hooks/category/useCreateCategory"

const initialValues: IAddCategory = {
  name: "",
  services: []
}

const initialErrors = {
  name: "",
  services: []
}

const AddCategoryScreen = () => {
  const {
    isValid,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
  } = useFormik({
    initialValues,
    initialErrors,
    validationSchema: AddCategorySchema,
    onSubmit: () => onSubmit(),
  })

  const { isLoading, runRequest } = useCreateCategory({})

  const onSubmit = () => {
    runRequest(values)
  }
  return (
    <View>
      <Text>AddServiceScreen</Text>
      <Field
        required
        name="name"
        label="Name"
        placeholder="Enter your name"
        value={values.name}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <ServicePicker />
      <Button
        disabled={!isValid || isLoading}
        title="Submit"
        onPress={() => handleSubmit()}
      />
    </View>
  )
}

export default AddCategoryScreen
