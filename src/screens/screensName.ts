export enum COMMON_SCREENS {
  welcome = "welcome",
}

export enum AUTH_SCREENS {
  login = "login",
  register = "register",
  forgot_password = "forgot_password",
  store_form = "store_form",
  search_store = "search_store",
}

export enum MAIN_SCREENS {
  tabNavigator = "tabNavigator",
  home = "home",
  customers = "customers",
  add_customer = "add_customer",
  import_customer = "import_customer",
  report = "report",
  notifications = "notifications",
  settings = "settings",
  profile = "profile",
  add_staff = "add_staff",
  staff_service = "staff_service",
  categories = "categories",
  staff = "staff",
}
