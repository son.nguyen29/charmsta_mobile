import { useNavigation } from "@react-navigation/native"
import React from "react"
import { Button, Text, View } from "react-native"

import { Field } from "@components/Field"
import { useLogin } from "@hooks/auth/useLogin"
import { useSocialSignIn } from "@hooks/auth/useSocialSignIn"
import { AuthNavigationProp } from "@navigators/auth"
import { AUTH_SCREENS } from "@screens/screensName"
import { colors } from "@theme/colors"
import { useFormik } from "formik"
import Ionicons from "react-native-vector-icons/Ionicons"
import LoginSchema from "./LoginSchema"
import { styles } from "./styles"

const initialValues: { email: string; password: string } = {
  email: "",
  password: "",
}

const initialErrors = {
  email: "",
  password: "",
}

const LoginScreen = () => {
  const navigation = useNavigation<AuthNavigationProp>()
  const { runRequest: login, isLoading } = useLogin({})
  const { runRequest: sociallogin, isLoading: socialLoading } = useSocialSignIn(
    {},
  )
  const {
    isValid,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
  } = useFormik({
    initialValues,
    initialErrors,
    validationSchema: LoginSchema,
    onSubmit: () => onLogin(),
  })

  const gotoRegister = () => {
    navigation.navigate(AUTH_SCREENS.register)
  }

  const onLogin = () => {
    login(values)
  }

  const onSocialPress = (name: "google" | "apple" | "facebook") => {
    sociallogin(name)
  }

  return (
    <View>
      <Text>LoginScreen</Text>
      <Field
        required
        name="email"
        label="Email"
        placeholder="Enter your email"
        value={values.email}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Field
        required
        isPassword
        name="password"
        label="Password"
        placeholder="Enter your password"
        value={values.password}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Button
        disabled={!isValid}
        title={isLoading ? "Login...." : "Login"}
        onPress={handleSubmit}
      />
      <View style={styles.socialBtnContainer}>
        <Ionicons
          color={colors.palette.white}
          style={styles.icon}
          size={24}
          name="logo-google"
          onPress={() => onSocialPress("google")}
        />
        <Ionicons
          color={colors.palette.white}
          style={styles.icon}
          size={24}
          name="logo-facebook"
          onPress={() => onSocialPress("facebook")}
        />
      </View>
      <Button title="Register" onPress={gotoRegister} />
    </View>
  )
}

export default LoginScreen
