import { colors } from "@theme/colors"
import { Dimensions, StyleSheet } from "react-native"
const { width } = Dimensions.get("window")

export const styles = StyleSheet.create({
  icon: { backgroundColor: colors.palette.black, borderRadius: 24, padding: 8 },
  socialBtnContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginBottom: 32,
    marginTop: "5%",
    width: width / 2.5,
  },
})
