import React from "react"
import { View, Text, FlatList, ActivityIndicator } from "react-native"
import { Button } from "d4dpocket"
import { useNavigation } from "@react-navigation/native"

import TextInput from "./TextInput"
import { Empty } from "@components/index"
import { AuthNavigationProp } from "@navigators/auth"
import { useStores } from "@models/index"
import { IStoreRegister } from "@models/store/StoreStore"
import { useSearchStores } from "@hooks/store/useSearchStores"
import { useSearchZipcode } from "@hooks/store/useSearchZipcode"

const SearchStoreScreen = () => {
  const navigation = useNavigation<AuthNavigationProp>()
  const { isLoading, data, runRequest } = useSearchStores({})

  const { runRequest: runRequestZipcode } = useSearchZipcode({})
  const {
    storeStore: { setStoreRegister },
  } = useStores()

  const keyExtractor = (item: IStoreRegister) => item.place_id?.toString()

  const handleSelectStore = (item: IStoreRegister) => {
    setStoreRegister(item)
    runRequestZipcode(item.place_id)
    navigation.goBack()
  }

  const renderItem = ({ item }: { item: IStoreRegister }) => (
    <Button onPress={() => handleSelectStore(item)}>
      <Text>{item.structured_formatting.main_text}</Text>
      <Text>{item.structured_formatting.main_text}</Text>
      <Text>{item.structured_formatting.secondary_text}</Text>
    </Button>
  )

  const onSearch = (input: string) => {
    runRequest(input, "establishment")
  }

  return (
    <View>
      <Text>SearchStoreScreen</Text>
      <TextInput onSearch={onSearch} />
      <FlatList
        data={data}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        ListHeaderComponent={isLoading ? <ActivityIndicator /> : null}
        ListEmptyComponent={!isLoading ? <Empty /> : null}
      />
    </View>
  )
}

export default SearchStoreScreen
