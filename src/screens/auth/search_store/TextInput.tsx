import React, { useState, useCallback } from "react"
import { View, TextInput as RNTextInput } from "react-native"
import debounce from "lodash/debounce"

interface Props {
  onSearch: (text: string) => void
}

const TextInput = ({ onSearch }: Props) => {
  const [value, setValue] = useState("")

  const handleSearch = useCallback(
    debounce((text) => {
      onSearch(text)
    }, 500),
    [],
  )

  const onChangeText = (text: string) => {
    setValue(text)
    handleSearch(text)
  }

  return (
    <View>
      <RNTextInput
        value={value}
        placeholder="Store name"
        onChangeText={onChangeText}
      />
    </View>
  )
}

export default TextInput
