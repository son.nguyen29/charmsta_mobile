import { string, object, InferType, ObjectSchema } from "yup"

import { ForgotPassword } from "@models/request/Auth"

const ForgotPasswordSchema: ObjectSchema<ForgotPassword> = object({
  email: string().email("Enter a valid email.").required("Email is required."),
})

export type IForgotPassword = InferType<typeof ForgotPasswordSchema>
export default ForgotPasswordSchema
