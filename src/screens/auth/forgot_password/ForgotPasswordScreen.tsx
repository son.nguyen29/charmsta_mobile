import React from "react"
import { View, Text, Button, Alert } from "react-native"
import { useFormik } from "formik"

import ForgotPasswordSchema, { IForgotPassword } from "./ForgotPasswordSchema"
import { Field } from "@components/index"
import { useForgotPassword } from "@hooks/auth/useForgotPassword"

const initialValues: IForgotPassword = {
  email: "",
}

const initialErrors = {
  email: "",
}

const ForgotPasswordScreen = () => {
  const successCallback = () => {
    // @todo
  }

  const failureCallback = () => {
    Alert.alert("Error, please try again")
  }

  const { isLoading, runRequest } = useForgotPassword({
    successCallback,
    failureCallback,
  })

  const {
    isValid,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
  } = useFormik({
    initialValues,
    initialErrors,
    validationSchema: ForgotPasswordSchema,
    onSubmit: () => onSubmit(),
  })

  const onSubmit = () => {
    runRequest(values)
  }

  return (
    <View>
      <Text>ForgotPasswordScreen</Text>
      <Field
        required
        name="email"
        label="Email"
        placeholder="Enter your email"
        value={values.email}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Button
        disabled={!isValid || isLoading}
        title="Submit"
        onPress={() => handleSubmit()}
      />
    </View>
  )
}

export default ForgotPasswordScreen
