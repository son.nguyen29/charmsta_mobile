import { string, object, array, InferType, ObjectSchema } from "yup"

import { REGEXP } from "@config/constants"
import { StoreRegister } from "@models/request/Store"

const StoreFormSchema: ObjectSchema<StoreRegister> = object({
  name: string().required("Name is required"),
  address: string().required("Address is required"),
  city: string().required("City is required"),
  state: string().required("State is required"),
  zipcode: string().required("Zipcode is required"),
  categories: array()
    .min(1, "Categories is required")
    .required("Categories is required"),
  phoneNumber: string()
    .matches(REGEXP.phoneNumber, "Phone number is not correct")
    .required("Phone number is required"),
})

export type IStoreForm = InferType<typeof StoreFormSchema>
export default StoreFormSchema
