import React, { useCallback, useState, useEffect } from "react"
import { View, Text, Button, Alert } from "react-native"
import { useFormik } from "formik"
import { useFocusEffect, useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"

import StoreFormSchema, { IStoreForm } from "./StoreFormSchema"
import { Field, MultiSelect } from "@components/index"
import { AUTH_SCREENS } from "@screens/screensName"
import { AuthNavigationProp } from "@navigators/auth"
import { useStores } from "@models/index"
import { useRegister } from "@hooks/auth/useRegister"
import { STORE_CATEGORIES } from "@config/constants"

const defaultValues: IStoreForm = {
  name: "",
  categories: [{ id: 1, name: "Category 1" }],
  address: "",
  city: "",
  state: "",
  zipcode: "",
  phoneNumber: "",
}

const initialErrors = {
  name: "",
  categories: "",
  address: "",
  city: "",
  state: "",
  zipcode: "",
  phoneNumber: "",
}

const StoreFormScreen = () => {
  const {
    storeStore: { storeRegister, zipcode },
  } = useStores()
  const navigation = useNavigation<AuthNavigationProp>()
  const [initialValues, setInitialValues] = useState<IStoreForm>(defaultValues)
  const {
    isValid,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
  } = useFormik({
    enableReinitialize: true,
    initialValues,
    initialErrors,
    validationSchema: StoreFormSchema,
    onSubmit: () => onSubmit(),
  })

  const successCallback = () => {
    // @todo
    // goto HomeScreen
  }

  const failureCallback = () => {
    Alert.alert("Error, please try again")
  }

  const { isLoading, runRequest } = useRegister({
    successCallback,
    failureCallback,
  })

  useFocusEffect(
    useCallback(() => {
      if (storeRegister) {
        const { terms, structured_formatting } = storeRegister
        const { main_text, secondary_text } = structured_formatting
        setInitialValues((prev) => ({
          ...prev,
          name: main_text,
          address: secondary_text,
          city: terms[2].value,
          state: terms[3].value,
        }))
      }
    }, [storeRegister]),
  )

  useEffect(() => {
    if (zipcode) {
      setInitialValues((prev) => ({ ...prev, zipcode }))
    }
  }, [zipcode])

  const onSubmit = () => {
    runRequest(values)
  }

  const gotoSearchStore = () => {
    navigation.navigate(AUTH_SCREENS.search_store)
  }

  return (
    <View>
      <Text>StoreFormScreen</Text>
      <Button title="Company Name" onPress={gotoSearchStore} />
      <MultiSelect
        titleModal="Categories"
        value={values.categories}
        data={STORE_CATEGORIES}
        onSubmit={(item) => setFieldValue("categories", item)}
      >
        <View>
          <Text>Categories</Text>
        </View>
      </MultiSelect>
      <Field
        required
        name="address"
        label="Address"
        placeholder="Enter your address"
        value={values.address}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Field
        required
        name="city"
        label="City"
        placeholder="Enter your city"
        value={values.city}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Field
        required
        name="state"
        label="State"
        placeholder="Enter your state"
        value={values.state}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Field
        required
        name="zipcode"
        label="Zipcode"
        keyboardType="numeric"
        placeholder="Enter your zipcode"
        value={values.zipcode}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Field
        required
        name="phoneNumber"
        label="Phone number"
        keyboardType="numeric"
        placeholder="Enter your phone number"
        value={values.phoneNumber}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Button
        disabled={!isValid || isLoading}
        onPress={() => handleSubmit()}
        title={isLoading ? "Waiting..." : "Submit"}
      />
    </View>
  )
}

export default observer(StoreFormScreen)
