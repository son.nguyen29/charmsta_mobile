import React from "react"
import { View, Text, Button } from "react-native"
import { useFormik } from "formik"
import { useNavigation } from "@react-navigation/native"

import RegisterSchema, { IRegister } from "./RegisterSchema"
import { Field } from "@components/Field"
import { AUTH_SCREENS } from "@screens/screensName"
import { AuthNavigationProp } from "@navigators/auth"
import { useStores } from "@models/index"

const initialValues: IRegister = {
  fullName: "",
  email: "",
  password: "",
  confirmPassword: "",
}

const initialErrors = {
  fullName: "",
  email: "",
  password: "",
  confirmPassword: "",
}

const RegisterScreen = () => {
  const navigation = useNavigation<AuthNavigationProp>()
  const {
    authStore: { setUserRegister },
  } = useStores()
  const {
    isValid,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
  } = useFormik({
    initialValues,
    initialErrors,
    validationSchema: RegisterSchema,
    onSubmit: () => onSubmit(),
  })

  const onSubmit = () => {
    setUserRegister(values)
    navigation.navigate(AUTH_SCREENS.store_form)
  }

  return (
    <View>
      <Text>RegisterScreen</Text>
      <Field
        required
        name="fullName"
        label="Fullname"
        placeholder="Enter your fullname"
        autoCapitalize="words"
        value={values.fullName}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Field
        required
        name="email"
        label="Email"
        placeholder="Enter your email"
        value={values.email}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Field
        required
        isPassword
        name="password"
        label="Password"
        placeholder="Enter your password"
        value={values.password}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Field
        required
        isPassword
        name="confirmPassword"
        label="Confirm password"
        placeholder="Password confirmation"
        value={values.confirmPassword}
        {...{ handleChange, handleBlur, errors, touched }}
      />
      <Button disabled={!isValid} onPress={() => handleSubmit()} title="Next" />
    </View>
  )
}

export default RegisterScreen
