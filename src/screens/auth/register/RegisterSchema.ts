import { string, object, ref, InferType, ObjectSchema } from "yup"

import { REGEXP } from "@config/constants"
import { UserRegister } from "@models/request/Auth"

const RegisterSchema: ObjectSchema<UserRegister & { confirmPassword: string }> =
  object({
    fullName: string().trim().min(2, "Too short").required("Name is required."),
    email: string()
      .email("Enter a valid email.")
      .required("Email is required."),
    password: string()
      .matches(
        REGEXP.password,
        "Password length must be between 6 and 12 characters, at least one character and one number",
      )
      .required("Password is required."),
    confirmPassword: string()
      .oneOf([ref("password"), ""], "Passwords must match")
      .required("Confirmation password is required."),
  })

export type IRegister = InferType<typeof RegisterSchema>
export default RegisterSchema
