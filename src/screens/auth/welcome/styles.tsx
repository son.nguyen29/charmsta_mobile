import { Dimensions, StyleSheet } from "react-native"

const { height, width } = Dimensions.get("screen")

const black = "#000"
const white = "#fff"
const whiteAlpha = "#00000025"

export const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: white,
    flex: 1,
    justifyContent: "center",
  },
  btnSkip: {
    alignItems: "center",
    backgroundColor: whiteAlpha,
    borderRadius: 25,
    height: 50,
    justifyContent: "center",
    position: "absolute",
    right: 20,
    top: 20,
    width: 50,
  },
  btnArea: {
    bottom: 20,
    left: 20,
    right: 20,
    top: 20,
  },
  body: {
    alignItems: "center",
    justifyContent: "center",
    marginVertical: 0,
    paddingVertical: 30,
    width,
  },
  topView: { justifyContent: "center", marginBottom: 20 },
  img: { height: width / 3, resizeMode: "contain", width: width / 3 },
  lblTitle: {
    color: black,
    fontSize: 24,
    fontWeight: "800",
    marginBottom: 10,
    textAlign: "center",
    textAlignVertical: "center",
  },
  lblDes: {
    color: black,
    fontWeight: "300",
    textAlign: "center",
    textAlignVertical: "center",
  },
  square: {
    backgroundColor: white,
    borderRadius: 86,
    height,
    left: -height * 0.3,
    position: "absolute",
    top: -height * 0.6,
    width: height,
  },
  flatlistContent: { paddingBottom: 0 },
  indicator: {
    backgroundColor: white,
    borderRadius: 5,
    height: 10,
    margin: 10,
    width: 10,
  },
  indicatorView: { bottom: "5%", flexDirection: "row", position: "absolute" },
})
