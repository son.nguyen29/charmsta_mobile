const possible =
  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

const random = (length = 6, str = possible) => {
  let text = ""
  for (let i = 0; i < length; i += 1) {
    text += str.charAt(Math.floor(Math.random() * str.length))
  }
  return text
}

export default random
