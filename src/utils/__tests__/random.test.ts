import random from "../random"

describe("utils random", () => {
  it("random characters default", () => {
    const randomVal = random()
    expect(randomVal.length).toBe(6)
  })

  it("random characters by length", () => {
    const randomVal = random(20)
    expect(randomVal.length).toBe(20)
  })

  it("random characters by other string", () => {
    const randomVal = random(5, "0123456789")
    expect(randomVal.length).toBe(5)
  })
})
