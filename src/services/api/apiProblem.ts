import { AxiosError } from "axios"

export const handleErrorResponse = (error: AxiosError): Promise<any> => {
  if (error.response) {
    // handle server error response
    console.log("Server Error", error.response)
  } else if (error.request) {
    // handle request error
    console.log("Request Error", error.request)
  } else {
    // handle other errors
    console.log("Error", error.message)
  }
  return Promise.reject(error)
}
