import { CreateCustomer, ImportCustomer } from "@models/request/Customer"
import { CustomerDTO } from "@models/response/Customer"
import api from ".."

const GET_CUSTOMERS = "/customers"
const CREATE_CUSTOMER = "/customers"
const IMPORT_CUSTOMER = "/customers/import"

export function getCustomersApi(
  params: {
    page?: number
    size?: number
    search?: string
    skip?: number
    take?: number
  } = {
    page: 0,
    size: 0,
    search: "",
    skip: 0,
    take: 0,
  },
) {
  return api<CustomerDTO[]>(GET_CUSTOMERS, "GET", undefined, {}, params)
}

export const createCustomerApi = (customer: CreateCustomer) =>
  api<CustomerDTO>(CREATE_CUSTOMER, "POST", customer)

export const importCustomerApi = (customers: ImportCustomer[]) =>
  api<{ status: "ok" }>(IMPORT_CUSTOMER, "POST", customers)
