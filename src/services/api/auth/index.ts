import {
  ForgotPassword,
  LoginEmail,
  SignUpEmail,
  SocialSignIn,
} from "@models/request/Auth"
import {
  LoginDTO,
  SignUpEmailDTO,
  SocialSignInDTO,
} from "@models/response/Auth"
import api from "../index"

const LOGIN = "/users/login"
const REGISTER = "/users/register"
const CONTINUE_WITH_GOOGLE = "/users/gglogin"
const CONTINUE_WITH_FACEBOOK = "/users/fblogin"
const CONTINUE_WITH_APPLE = "/users/applelogin"
const FORGOT_PASSWORD = "/users/forgot"

export function loginApi(data: LoginEmail) {
  return api<LoginDTO>(LOGIN, "POST", data)
}

export function registerApi(data: SignUpEmail) {
  return api<SignUpEmailDTO>(REGISTER, "POST", data)
}

export function continueWithGoogleApi(data: SocialSignIn, token: string) {
  return api<SocialSignInDTO>(CONTINUE_WITH_GOOGLE, "POST", data, {
    Authorization: "Bearer " + token,
  })
}

export function continueWithFacebookApi(data: SocialSignIn, token: string) {
  return api<SocialSignInDTO>(CONTINUE_WITH_FACEBOOK, "POST", data, {
    Authorization: "Bearer " + token,
  })
}

export function continueWithAppleApi(data: SocialSignIn, token: string) {
  return api<SocialSignInDTO>(CONTINUE_WITH_APPLE, "POST", data, {
    Authorization: "Bearer " + token,
  })
}

export function forgotPasswordApi(params: ForgotPassword) {
  return api<any>(`${FORGOT_PASSWORD}?email=${params.email}`, "GET")
}
