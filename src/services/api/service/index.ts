import { AddService } from "@models/request/Service"
import { ServiceDTO } from "@models/response/Service"
import api from "../index"

const ADD_SERVICE = "/appointment/service"
const GET_SERVICE = "/appointment/service"

export function addServiceApi(data: AddService) {
  return api<ServiceDTO>(ADD_SERVICE, "POST", data)
}

export function getServiceApi() {
  return api<ServiceDTO[]>(GET_SERVICE, "GET")
}
