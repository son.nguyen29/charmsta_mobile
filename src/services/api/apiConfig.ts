import axios from "axios"
import axiosRetry from "axios-retry"
import Config from "react-native-config"

const axiosInstance = axios.create({
  baseURL: Config.API_URL,
  withCredentials: true,
  timeout: 10000, // 10 seconds
})

axiosRetry(axiosInstance, {
  retries: 3,
  retryDelay: (retryCount: number): number => retryCount * 1000,
})

export default axiosInstance
