import { UploadDTO } from "@models/response/Utils"
import api from ".."

const UPLOAD = "/upload"

export const uploadApi = (data: FormData) =>
  api<UploadDTO>(UPLOAD, "POST", data, {
    "Content-Type": "multipart/form-data",
  })
