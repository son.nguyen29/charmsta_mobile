import axios from "axios"

import { PlaceDetailsDTO } from "@models/response/Store"
import { IStoreRegister } from "@models/store/StoreStore"
import { googleServices } from "@config/config"
import { PLACE_AUTOCOMPLETE, PLACE_DETAILS } from "@config/constants"

export const getStoresPrediction = (
  input: string,
  type: "establishment" | "geocode" = "geocode",
) => {
  return axios.get<{ predictions: IStoreRegister[] }>(PLACE_AUTOCOMPLETE, {
    params: {
      key: googleServices.googleKey,
      input,
      type: type,
      maximumAge: 0,
    },
  })
}

export const getPlaceDetails = (placeid: string) => {
  return axios.get<PlaceDetailsDTO>(PLACE_DETAILS, {
    params: {
      key: googleServices.googleKey,
      placeid,
      fields: "name,address_components",
    },
  })
}
