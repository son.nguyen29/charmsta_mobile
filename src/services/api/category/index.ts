import { AddCategory } from "@models/request/Category"
import { CategoryDTO } from "@models/response/Category"
import api from "../index"

const ADD_CATEGORY = "/pop/category"
const CATEGORY_LIST = "/pop/category"

export function addCategoryApi(data: AddCategory) {
  return api<CategoryDTO>(ADD_CATEGORY, "POST", data)
}


export function getCategoriesApi() {
  return api<CategoryDTO[]>(CATEGORY_LIST, "GET")
}
