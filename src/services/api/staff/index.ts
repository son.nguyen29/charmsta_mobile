import { AddStaff } from "@models/request/Staff"
import {
  PaginationStaffDTO,
  StaffDTO,
  StaffByServiceDTO,
} from "@models/response/Staff"
import api from "../index"

export interface ParamsStaff {
  page: number
  size: number
  search?: string
}

export interface ParamsStaffService {
  search?: string
}

const GET_STAFF = "/appointment/staff"
const CREATE_STAFF = "/appointment/staff"
const GET_STAFF_BY_SERVICE = (serviceId: number) =>
  `/appointment/service/${serviceId}/staffs`

export const getStaffApi = (params: ParamsStaff) =>
  api<PaginationStaffDTO>(GET_STAFF, "GET", undefined, {}, params)

export const createStaffApi = (data: AddStaff) =>
  api<StaffDTO>(CREATE_STAFF, "POST", data)

export const getStaffByServiceApi = (
  serviceId: number,
  params?: ParamsStaffService,
) =>
  api<StaffByServiceDTO[]>(
    GET_STAFF_BY_SERVICE(serviceId),
    "GET",
    null,
    null,
    params,
  )
