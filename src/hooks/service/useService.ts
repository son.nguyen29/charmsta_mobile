import { useState, useEffect } from "react"
import { AxiosError } from "axios"

import { ServiceDTO } from "@models/response/Service"
import { getServiceApi } from "@services/api/service"

export interface HookInterface {
  failureCallback?: (err: AxiosError | any) => void
  successCallback?: () => void
}

type ReturnDataType = {
  isLoading: boolean
  errors: AxiosError
  data: ServiceDTO[]
  runRequest: () => void
}

export const useService: (args: HookInterface) => ReturnDataType = ({
  failureCallback: componentFailureCallback,
  successCallback: componentSuccessCallback,
}) => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<AxiosError | any>()
  const [data, setData] = useState<ServiceDTO[]>([])

  const runRequest = async () => {
    try {
      setLoading(true)
      const response = await getServiceApi()
      if (response.data) {
        setLoading(false)
        setData(response.data)
        componentSuccessCallback && componentSuccessCallback()
      }
    } catch (error) {
      setLoading(false)
      setErrors(error)
      componentFailureCallback && componentFailureCallback(error)
    }
  }

  useEffect(() => {
    runRequest()
  }, [])

  const returnObject: ReturnDataType = {
    isLoading,
    data,
    errors,
    runRequest,
  }

  return returnObject
}
