import { useState } from "react"
import { AxiosError } from "axios"

import { AddService } from "@models/request/Service"
import { addServiceApi } from "@services/api/service"

export interface HookInterface {
  failureCallback?: (err: AxiosError | any) => void
  successCallback?: () => void
}

type ReturnDataType = {
  isLoading: boolean
  errors: AxiosError
  runRequest: (params: AddService) => void
}

export const useCreateService: (args: HookInterface) => ReturnDataType = ({
  failureCallback: componentFailureCallback,
  successCallback: componentSuccessCallback,
}) => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<AxiosError | any>()

  const runRequest = async (data: AddService) => {
    try {
      setLoading(true)
      const response = await addServiceApi(data)
      if (response.data) {
        setLoading(false)
        componentSuccessCallback && componentSuccessCallback()
      }
    } catch (error) {
      setLoading(false)
      setErrors(error)
      componentFailureCallback && componentFailureCallback(error)
    }
  }

  const returnObject: ReturnDataType = {
    isLoading,
    errors,
    runRequest,
  }

  return returnObject
}
