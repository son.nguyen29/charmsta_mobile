import { renderHook, act } from "@testing-library/react-hooks"

import * as authApi from "@services/api/auth"
import { useRegister } from "../auth/useRegister"
import data from "./data"

const failureCallback = jest.fn()

const successCallback = jest.fn()

const mockResponse = { data: {} }

describe("test useRegister", () => {
  const spyOn = jest.spyOn(authApi, "registerApi")

  it("should return initial state", () => {
    const { result } = renderHook(() => useRegister({}))
    expect(result.current.isLoading).toBe(false)
    expect(result.current.errors).toBeUndefined()
    expect(result.current.runRequest).toBeInstanceOf(Function)
  })

  it("should loading correctly", async () => {
    // @ts-ignore
    spyOn.mockResolvedValue(mockResponse)
    const { result } = renderHook(() => useRegister({}))
    expect(result.current.isLoading).toEqual(false)
    await act(async () => {
      result.current.runRequest(data.randomStore())
    })
    expect(result.current.isLoading).toEqual(false)
  })

  it("should call failureCallback missing field data", async () => {
    // @ts-ignore
    spyOn.mockRejectedValue(mockResponse)
    const { result } = renderHook(() =>
      useRegister({
        failureCallback,
        successCallback,
      }),
    )
    await act(async () => result.current.runRequest(data.randomStore()))
    expect(failureCallback).toHaveBeenCalled()
  })

  it("should call successCallback", async () => {
    // @ts-ignore
    spyOn.mockResolvedValue(mockResponse)
    const { result } = renderHook(() =>
      useRegister({
        failureCallback,
        successCallback,
      }),
    )
    await act(async () => await result.current.runRequest(data.randomStore()))
    expect(successCallback).toHaveBeenCalled()
  })
})
