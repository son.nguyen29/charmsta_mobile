import { renderHook, act } from "@testing-library/react-hooks"

import * as staffApi from "@services/api/staff"
import { useCreateStaff } from "../staff/useCreateStaff"
import data from "./data"

const mockResponse = { data: {} }

const successCallback = jest.fn()

const failureCallback = jest.fn()

describe("test useCreateStaff", () => {
  const spyOn = jest.spyOn(staffApi, "createStaffApi")

  it("should return initial state", async () => {
    // @ts-ignore
    spyOn.mockResolvedValue(mockResponse)
    const { result } = renderHook(() => useCreateStaff({}))
    expect(result.current.errors).toBeUndefined()
    expect(result.current.isLoading).toBe(false)
    expect(result.current.runRequest).toBeInstanceOf(Function)
  })

  it("should call failureCallback", async () => {
    // @ts-ignore
    spyOn.mockRejectedValue(mockResponse)
    const { result } = renderHook(() =>
      useCreateStaff({ successCallback, failureCallback }),
    )
    await act(async () => {
      result.current.runRequest(data.randomStaff())
    })
    expect(failureCallback).toHaveBeenCalled()
  })

  it("should call successCallback", async () => {
    // @ts-ignore
    spyOn.mockResolvedValue(mockResponse)
    const { result } = renderHook(() =>
      useCreateStaff({ successCallback, failureCallback }),
    )
    await act(async () => {
      result.current.runRequest(data.randomStaff())
    })
    expect(successCallback).toHaveBeenCalled()
  })
})
