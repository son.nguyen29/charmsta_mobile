import random from "@utils/random"
import { UserRegister } from "@models/request/Auth"
import { StoreRegister } from "@models/request/Store"
import { AddService } from "@models/request/Service"
import { AddStaff } from "@models/request/Staff"

const randomVal = random()

const data = {
  placeid: "ChIJz3KubQsJK4cRGVoEHV1TLeo",
  email: "tuantv@gmail.com",
  password: "123456a",
  randomStore: (): StoreRegister => {
    return {
      name: `store${randomVal}`,
      address: `store address ${randomVal}`,
      city: `store city ${randomVal}`,
      state: `store state ${randomVal}`,
      zipcode: random(6, "0123456789"),
      categories: [{ id: "1", name: "Test category" }],
      phoneNumber: random(10, "0123456789"),
    }
  },
  randomUser: (): UserRegister => {
    return {
      fullName: `user${randomVal}`,
      email: `user${randomVal}@gmail.com`,
      password: "123456a",
    }
  },
  randomService: (): AddService => {
    return {
      name: `service${randomVal}`,
      serviceDuration: "360",
      cost: "120",
      category: null,
      staff: [],
    }
  },
  randomStaff: (): AddStaff => {
    return {
      avatar: "",
      name: `staff${randomVal}`,
      phoneNumber: random(10, "0123456789"),
      email: `staff${randomVal}@gmail.com`,
      break: [{ id: 1 }],
      workingDay: [{ id: 1 }],
      permission: 1,
    }
  },
}

export default data
