import { renderHook, act } from "@testing-library/react-hooks"

import * as serviceApi from "@services/api/service"
import { useCreateService } from "../service/useCreateService"
import data from "./data"

const mockResponse = { data: {} }

const successCallback = jest.fn()

const failureCallback = jest.fn()

describe("test useCreateService", () => {
  const spyOn = jest.spyOn(serviceApi, "addServiceApi")

  it("should return initial state", () => {
    const { result } = renderHook(() => useCreateService({}))
    expect(result.current.isLoading).toBe(false)
    expect(result.current.errors).toBeUndefined()
    expect(result.current.runRequest).toBeInstanceOf(Function)
  })

  it("should loading correctly", async () => {
    // @ts-ignore
    spyOn.mockResolvedValue(mockResponse)
    const { result } = renderHook(() => useCreateService({}))
    expect(result.current.isLoading).toBe(false)
    await act(async () => {
      result.current.runRequest(data.randomService())
    })
    expect(result.current.isLoading).toBe(false)
  })

  it("should call failureCallback", async () => {
    // @ts-ignore
    spyOn.mockRejectedValue(mockResponse)
    const { result } = renderHook(() =>
      useCreateService({ successCallback, failureCallback }),
    )
    await act(async () => {
      result.current.runRequest(data.randomService())
    })
    expect(failureCallback).toHaveBeenCalled()
  })

  it("should call successCallback", async () => {
    // @ts-ignore
    spyOn.mockResolvedValue(mockResponse)
    const { result } = renderHook(() =>
      useCreateService({ successCallback, failureCallback }),
    )
    await act(async () => {
      result.current.runRequest(data.randomService())
    })
    expect(successCallback).toHaveBeenCalled()
  })
})
