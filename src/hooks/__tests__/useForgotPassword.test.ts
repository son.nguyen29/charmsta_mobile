import { act, waitFor } from "@testing-library/react-native"
import { renderHook } from "@testing-library/react-hooks"

import { useForgotPassword } from "../auth/useForgotPassword"
import data from "./data"

describe("test useForgotPassword", () => {
  it("should return initial state", () => {
    const { result } = renderHook(() => useForgotPassword({}))
    expect(result.current.isLoading).toBe(false)
    expect(result.current.errors).toBeUndefined()
    expect(result.current.runRequest).toBeInstanceOf(Function)
  })

  it("should loading correctly", async () => {
    const { result, waitForNextUpdate } = renderHook(() =>
      useForgotPassword({}),
    )
    await act(async () => {
      result.current.runRequest({
        email: data.email,
      })
    })
    expect(result.current.isLoading).toBe(true)
    await waitForNextUpdate()
    expect(result.current.isLoading).toBe(false)
  })

  it("should call successCallback", async () => {
    const successCallback = jest.fn()
    const failureCallback = jest.fn()
    const { result } = renderHook(() =>
      useForgotPassword({ successCallback, failureCallback }),
    )
    await act(async () => {
      result.current.runRequest({
        email: data.email,
      })
    })
    await waitFor(() => {
      expect(successCallback).toHaveBeenCalled()
    })
  })
})
