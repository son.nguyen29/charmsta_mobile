import { renderHook } from "@testing-library/react-hooks"

import * as categoryApi from "@services/api/category"
import { useCategory } from "../category/useCategory"

const mockResponse = { data: {} }

const successCallback = jest.fn()

const failureCallback = jest.fn()

describe("test useCategory", () => {
  const spyOn = jest.spyOn(categoryApi, "getCategoriesApi")

  it("should return initial state", async () => {
    // @ts-ignore
    spyOn.mockResolvedValue(mockResponse)
    const { result, waitForNextUpdate } = renderHook(() =>
      useCategory({}),
    )
    await waitForNextUpdate()
    expect(result.current.isLoading).toBe(false)
    expect(result.current.errors).toBeUndefined()
    expect(result.current.runRequest).toBeInstanceOf(Function)
  })

  it("should call failureCallback", async () => {
    // @ts-ignore
    spyOn.mockRejectedValue(mockResponse)
    const { waitForNextUpdate } = renderHook(() =>
      useCategory({ successCallback, failureCallback }),
    )
    await waitForNextUpdate()
    expect(failureCallback).toHaveBeenCalled()
  })

  it("should call successCallback", async () => {
    // @ts-ignore
    spyOn.mockResolvedValue(mockResponse)
    const { waitForNextUpdate } = renderHook(() =>
      useCategory({ successCallback, failureCallback }),
    )
    await waitForNextUpdate()
    expect(successCallback).toHaveBeenCalled()
  })
})
