import { renderHook, act } from "@testing-library/react-hooks"

import * as staffApi from "@services/api/staff"
import { usStaffService } from "../staff/usStaffService"

const mockResponse = { data: {} }

const serviceId = 1

const successCallback = jest.fn()

const failureCallback = jest.fn()

describe("test usStaffService", () => {
  const spyOn = jest.spyOn(staffApi, "getStaffByServiceApi")

  it("should return initial state", async () => {
    // @ts-ignore
    spyOn.mockResolvedValue(mockResponse)
    const { result } = renderHook(() => usStaffService({}))
    expect(result.current.runRequest).toBeInstanceOf(Function)
    expect(result.current.errors).toBeUndefined()
    expect(result.current.isLoading).toBe(false)
  })

  it("should call failureCallback", async () => {
    // @ts-ignore
    spyOn.mockRejectedValue(mockResponse)
    const { result } = renderHook(() =>
      usStaffService({ successCallback, failureCallback }),
    )
    await act(async () => {
      result.current.runRequest(serviceId)
    })
    expect(failureCallback).toHaveBeenCalled()
  })

  it("should call successCallback", async () => {
    // @ts-ignore
    spyOn.mockResolvedValue(mockResponse)
    const { result } = renderHook(() =>
      usStaffService({ successCallback, failureCallback }),
    )
    await act(async () => {
      result.current.runRequest(serviceId)
    })
    expect(successCallback).toHaveBeenCalled()
  })
})
