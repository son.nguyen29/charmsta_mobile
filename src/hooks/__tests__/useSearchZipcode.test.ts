import { act, waitFor } from "@testing-library/react-native"
import { renderHook } from "@testing-library/react-hooks"

import { useSearchZipcode } from "../store/useSearchZipcode"
import data from "./data"

describe("test useSearchZipcode", () => {
  it("should return initial state", () => {
    const { result } = renderHook(() => useSearchZipcode({}))
    expect(result.current.isLoading).toBe(false)
    expect(result.current.errors).toBeUndefined()
    expect(result.current.runRequest).toBeInstanceOf(Function)
  })

  it("should loading correctly", async () => {
    const { result, waitForNextUpdate } = renderHook(() => useSearchZipcode({}))
    await act(async () => {
      result.current.runRequest(data.placeid)
    })
    expect(result.current.isLoading).toBe(true)
    await waitForNextUpdate()
    expect(result.current.isLoading).toBe(false)
  })

  it("should call successCallback", async () => {
    const successCallback = jest.fn()
    const failureCallback = jest.fn()
    const { result } = renderHook(() =>
      useSearchZipcode({ successCallback, failureCallback }),
    )
    await act(async () => {
      result.current.runRequest(data.placeid)
    })
    await waitFor(() => {
      expect(successCallback).toHaveBeenCalled()
    })
  })
})
