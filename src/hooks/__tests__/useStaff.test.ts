import { renderHook } from "@testing-library/react-hooks"

import * as staffApi from "@services/api/staff"
import { useStaff } from "../staff/useStaff"

const mockResponse = { data: {} }

const successCallback = jest.fn()

const failureCallback = jest.fn()

describe("test useStaff", () => {
  const spyOn = jest.spyOn(staffApi, "getStaffApi")

  it("should return initial state", async () => {
    // @ts-ignore
    spyOn.mockResolvedValue(mockResponse)
    const { result, waitForNextUpdate } = renderHook(() => useStaff({}))
    expect(result.current.runRequest).toBeInstanceOf(Function)
    expect(result.current.errors).toBeUndefined()
    expect(result.current.isLoading).toBe(true)
    await waitForNextUpdate()
    expect(result.current.isLoading).toBe(false)
  })

  it("should call failureCallback", async () => {
    // @ts-ignore
    spyOn.mockRejectedValue(mockResponse)
    const { waitForNextUpdate } = renderHook(() =>
      useStaff({ successCallback, failureCallback }),
    )
    await waitForNextUpdate()
    expect(failureCallback).toHaveBeenCalled()
  })

  it("should call successCallback", async () => {
    // @ts-ignore
    spyOn.mockResolvedValue(mockResponse)
    const { waitForNextUpdate } = renderHook(() =>
      useStaff({ successCallback, failureCallback }),
    )
    await waitForNextUpdate()
    expect(successCallback).toHaveBeenCalled()
  })
})
