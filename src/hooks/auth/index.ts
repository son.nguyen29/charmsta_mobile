import { useState } from "react"

import { translate } from "@i18n/translate"
import { useStores } from "@models/index"
import { LoginEmail } from "@models/request/Auth"
import { navigate } from "@navigators/navigationUtilities"
import { AUTH_SCREENS } from "@screens/screensName"
import { loginApi } from "@services/api/auth"
import { getDeviceInfo } from "@utils/deviceInfo"
import { AxiosResponse } from "axios"
import _ from "lodash"
import { Alert } from "react-native"

interface Output {
  isLoading: boolean
  errors: Error
  login: (data: LoginEmail, callback: () => void) => void
}

export const useAuth = (): Output => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<any>(null) // @todo
  const {
    authStore: { setAuthToken },
  } = useStores()

  const login = async (data: LoginEmail, callback?: () => void) => {
    try {
      const request = {
        email: data.email.trim(),
        password: data.password,
      } as LoginEmail
      const { deviceToken } = await getDeviceInfo()
      const response = await loginApi({ ...request, deviceToken })
      if (response?.data) {
        const { deviceToken } = response.data
        setLoading(false)
        setAuthToken(deviceToken)
      }
    } catch (error) {
      if (!_.isEmpty(error)) {
        const { status } = error as AxiosResponse

        switch (status) {
          case 403:
            return Alert.alert(
              "Error",
              "Your password is not correct, please try another",
            )
          case 400:
            return Alert.alert(
              "Warning",
              "Your account has been deleted, please contact support for more infomation",
            )
          case 503:
          case 500:
            return Alert.alert("Error", "Your email is not available", [
              { text: "Cancel" },
              {
                text: "Sign up",
                onPress: () => navigate(AUTH_SCREENS.register),
              },
            ])
          default:
            return Alert.alert("Error", translate("errors.unexpected"))
        }
      }
      setErrors(error)
      setLoading(false)
    } finally {
    }
  }

  const register = async (data: any) => {
    // @todo
    setLoading(true)
    try {
      // const response = await registerApi(data)
    } catch (error) {
    } finally {
      setLoading(false)
    }
  }

  return { isLoading, errors, login, register }
}
