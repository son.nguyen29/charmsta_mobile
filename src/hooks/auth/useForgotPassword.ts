import { useState } from "react"
import { AxiosError } from "axios"

import { ForgotPassword } from "@models/request/Auth"
import { forgotPasswordApi } from "@services/api/auth"

export interface HookInterface {
  failureCallback?: (err: AxiosError | any) => void
  successCallback?: () => void
}

type ReturnDataType = {
  isLoading: boolean
  errors: AxiosError
  runRequest: (params: ForgotPassword) => void
}

export const useForgotPassword: (args: HookInterface) => ReturnDataType = ({
  failureCallback: componentFailureCallback,
  successCallback: componentSuccessCallback,
}) => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<AxiosError | any>()

  const runRequest = async (params: ForgotPassword) => {
    try {
      setLoading(true)
      const response = await forgotPasswordApi(params)
      if (response.data) {
        setLoading(false)
        componentSuccessCallback && componentSuccessCallback()
      }
    } catch (error) {
      setLoading(false)
      setErrors(error)
      componentFailureCallback && componentFailureCallback(error)
    }
  }

  const returnObject: ReturnDataType = {
    isLoading,
    errors,
    runRequest,
  }

  return returnObject
}
