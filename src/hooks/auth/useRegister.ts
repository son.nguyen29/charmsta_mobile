import { useState } from "react"
import { AxiosError } from "axios"

import { StoreRegister } from "@models/request/Store"
import { SignUpEmail } from "@models/request/Auth"
import { registerApi } from "@services/api/auth"
import { useStores } from "@models/index"
import { getDeviceInfo } from "@utils/deviceInfo"

export interface HookInterface {
  failureCallback?: (err: AxiosError | any) => void
  successCallback?: () => void
}

type ReturnDataType = {
  isLoading: boolean
  errors: AxiosError
  runRequest: (params: StoreRegister) => Promise<void>
}

export const useRegister: (args: HookInterface) => ReturnDataType = ({
  failureCallback: componentFailureCallback,
  successCallback: componentSuccessCallback,
}) => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<AxiosError | any>()
  const {
    authStore: { userRegister },
  } = useStores()

  const runRequest = async (store: StoreRegister) => {
    try {
      setLoading(true)
      const { deviceToken } = await getDeviceInfo()
      const request = {
        fullName: userRegister.fullName,
        email: userRegister.email,
        password: userRegister.password,
        deviceToken,
        store,
      } as SignUpEmail
      const response = await registerApi(request)
      if (response.data) {
        setLoading(false)
        componentSuccessCallback && componentSuccessCallback()
      }
    } catch (error) {
      setLoading(false)
      setErrors(error)
      componentFailureCallback && componentFailureCallback(error)
    }
  }

  const returnObject: ReturnDataType = {
    isLoading,
    errors,
    runRequest,
  }

  return returnObject
}
