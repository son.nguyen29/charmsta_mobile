import { translate } from "@i18n/translate"
import { useStores } from "@models/index"
import { SocialSignIn } from "@models/request/Auth"
import { SocialSignInDTO } from "@models/response/Auth"
import auth, { FirebaseAuthTypes } from "@react-native-firebase/auth"
import { GoogleSignin } from "@react-native-google-signin/google-signin"
import {
  continueWithFacebookApi,
  continueWithGoogleApi,
} from "@services/api/auth"
import { getDeviceInfo } from "@utils/deviceInfo"
import { AxiosError } from "axios"
import { isFunction } from "lodash"
import { useState } from "react"
import { Alert } from "react-native"
import { AccessToken, LoginManager } from "react-native-fbsdk-next"
import { LoginHookInterface } from "./useLogin"
type ReturnDataType = {
  isLoading: boolean
  error: AxiosError
  runRequest: (name: "google" | "apple" | "facebook") => void
}

export const useSocialSignIn: (args: LoginHookInterface) => ReturnDataType = ({
  successCallback,
  failureCallback,
}) => {
  const [isLoading, setLoading] = useState(false)
  const [error, setError] = useState<AxiosError | any>()
  const {
    authStore: { setAuthToken },
  } = useStores()

  const getAuthCredential = async (
    name: "google" | "apple" | "facebook",
  ): Promise<FirebaseAuthTypes.AuthCredential | any> => {
    try {
      let credential = {} as FirebaseAuthTypes.AuthCredential
      switch (name) {
        case "facebook":
          await LoginManager.logInWithPermissions(["email", "public_profile"])

          const response = await AccessToken.getCurrentAccessToken()
          const facebookCredential = auth.FacebookAuthProvider.credential(
            response?.accessToken || "N/A",
          )
          credential = facebookCredential
          break
        case "google":
          const { idToken } = await GoogleSignin.signIn()
          const googleCredential = auth.GoogleAuthProvider.credential(idToken)
          credential = googleCredential
          break
      }

      return credential
    } catch (err) {
      Alert.alert("Error", translate("errors.unexpected"))
    }
  }

  const runRequest = async (name: "google" | "apple" | "facebook") => {
    try {
      setLoading(true)
      const { deviceToken } = await getDeviceInfo()
      const credential = await getAuthCredential(name)
      const userData = await auth().signInWithCredential(credential)
      if (userData?.user) {
        const idToken = (await auth().currentUser?.getIdToken(true)) || "N/A"
        let response = {} as { data: SocialSignInDTO; code?: number }
        const userJSON = userData.user.toJSON()
        const body = Object.assign(userJSON, { deviceToken })
        switch (name) {
          case "facebook":
            response = await continueWithFacebookApi(
              body as SocialSignIn,
              idToken,
            )
            break
          case "google":
            response = await continueWithGoogleApi(
              body as SocialSignIn,
              idToken,
            )
            break
        }
        if (response?.data) {
          setAuthToken(response.data.accessToken.token)
          isFunction(successCallback) && successCallback()
          setLoading(false)
        }
      }
    } catch (err) {
      Alert.alert("Error", translate("errors.unexpected"))
      isFunction(failureCallback) && failureCallback(err)
      setLoading(false)
    }
  }

  return { isLoading, error, runRequest }
}
