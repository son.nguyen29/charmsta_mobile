import { translate } from "@i18n/translate"
import { useStores } from "@models/index"
import { LoginEmail } from "@models/request/Auth"
import { navigate } from "@navigators/navigationUtilities"
import { AUTH_SCREENS } from "@screens/screensName"
import { loginApi } from "@services/api/auth"
import { getDeviceInfo } from "@utils/deviceInfo"
import { saveString } from "@utils/storage"
import { AxiosError } from "axios"
import _, { isFunction } from "lodash"
import { useState } from "react"
import { Alert } from "react-native"

export interface LoginHookInterface {
  failureCallback?: (err: AxiosError | any) => void
  successCallback?: () => void
}

type ReturnDataType = {
  isLoading: boolean
  error: AxiosError
  runRequest: (params: LoginEmail) => void
}

export const useLogin: (args: LoginHookInterface) => ReturnDataType = ({
  successCallback,
  failureCallback,
}) => {
  const [isLoading, setLoading] = useState(false)
  const [error, setError] = useState<AxiosError | any>()
  const {
    authStore: { setAuthToken },
  } = useStores()

  const runRequest = async (data: LoginEmail) => {
    try {
      const request = {
        email: data.email.trim(),
        password: data.password,
      } as LoginEmail
      const { deviceToken } = await getDeviceInfo()
      const response = await loginApi({ ...request, deviceToken })

      if (response?.data) {
        const { accessToken } = response.data
        setLoading(false)
        isFunction(successCallback) && successCallback()
        saveString("@token", accessToken.token)
        setAuthToken(accessToken.token)
      }
    } catch (error) {
      if (!_.isEmpty(error)) {
        const { status } = error as AxiosError
        switch (status) {
          case 403:
            return Alert.alert(
              "Error",
              "Your password is not correct, please try another",
            )
          case 400:
            return Alert.alert(
              "Warning",
              "Your account has been deleted, please contact support for more infomation",
            )
          case 503:
          case 500:
            return Alert.alert("Error", "Your email is not available", [
              { text: "Cancel" },
              {
                text: "Sign up",
                onPress: () => navigate(AUTH_SCREENS.register),
              },
            ])
          default:
            return Alert.alert("Error", translate("errors.unexpected"))
        }
      }
      setLoading(false)
      isFunction(failureCallback) && failureCallback()
    }
  }

  return { isLoading, error, runRequest }
}
