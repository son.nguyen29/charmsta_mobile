import { ImportCustomer } from "@models/request/Customer"
import { importCustomerApi } from "@services/api/main"
import { AxiosError } from "axios"
import { isFunction } from "lodash"
import { useState } from "react"

interface ReturnDataType {
  isLoading: boolean
  error: AxiosError
  runRequest: (customers: ImportCustomer[]) => void
}

interface HookInterface {
  successCallback?: ({ status }: { status: string }) => void
  failureCallback?: (error: AxiosError | any) => void
}

export const useImportCustomers: (args: HookInterface) => ReturnDataType = ({
  failureCallback,
  successCallback,
}) => {
  const [isLoading, setLoading] = useState(false)
  const [error, setError] = useState<AxiosError | any>()

  const runRequest = async (customers: ImportCustomer[]) => {
    try {
      setLoading(true)
      const response = await importCustomerApi(customers)
      if (response?.data) {
        isFunction(successCallback) && successCallback(response?.data)
        setLoading(false)
      }
    } catch (err) {
      isFunction(failureCallback) && failureCallback(err)
      setError(err)
      setLoading(false)
    }
  }

  return { isLoading, error, runRequest }
}
