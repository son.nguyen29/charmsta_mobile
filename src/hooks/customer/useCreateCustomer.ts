import { CreateCustomer } from "@models/request/Customer"
import { CustomerDTO } from "@models/response/Customer"
import { createCustomerApi } from "@services/api/main"
import { AxiosError } from "axios"
import { isFunction } from "lodash"
import { useState } from "react"

interface ReturnDataType {
  isLoading: boolean
  error: AxiosError
  runRequest: (customer: CreateCustomer) => void
}

interface HookInterface {
  successCallback?: (customer: CustomerDTO) => void
  failureCallback?: (error: AxiosError | any) => void
}

export const useCreateCustomer: (args: HookInterface) => ReturnDataType = ({
  failureCallback,
  successCallback,
}) => {
  const [isLoading, setLoading] = useState(false)
  const [error, setError] = useState<AxiosError | any>()

  const runRequest = async (customer: CreateCustomer) => {
    try {
      setLoading(true)
      const response = await createCustomerApi(customer)
      if (response?.data) {
        isFunction(successCallback) && successCallback(response?.data)
        setLoading(false)
      }
    } catch (err) {
      isFunction(failureCallback) && failureCallback(err)
      setError(err)
      setLoading(false)
    }
  }

  return { isLoading, error, runRequest }
}
