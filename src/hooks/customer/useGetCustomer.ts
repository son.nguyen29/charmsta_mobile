import { CustomerDTO } from "@models/response/Customer"
import { getCustomersApi } from "@services/api/main"
import { AxiosError } from "axios"
import { isFunction } from "lodash"
import { useState } from "react"

interface ReturnDataType {
  isLoading: boolean
  error: AxiosError
  runRequest: (params?: {
    page?: number
    size?: number
    search?: string
    take?: number
    skip?: number
  }) => void
}

interface HookInterface {
  successCallback?: (customers: CustomerDTO[]) => void
  failureCallback?: (error: AxiosError | any) => void
}

export const useGetCustomers: (args: HookInterface) => ReturnDataType = ({
  failureCallback,
  successCallback,
}) => {
  const [isLoading, setLoading] = useState(false)
  const [error, setError] = useState<AxiosError | any>()

  const runRequest = async (
    args: {
      page?: number
      size?: number
      search?: string
      take?: number
      skip?: number
    } = {
      page: 0,
      size: 0,
      search: "",
      take: 0,
      skip: 0,
    },
  ) => {
    try {
      setLoading(true)
      const response = await getCustomersApi(args)
      if (response?.data) {
        isFunction(successCallback) && successCallback(response?.data)
        setLoading(false)
      }
    } catch (err) {
      isFunction(failureCallback) && failureCallback(err)
      setLoading(false)
    }
  }

  return { isLoading, error, runRequest }
}
