import { useState } from "react"
import { AxiosError } from "axios"

import { AddStaff } from "@models/request/Staff"
import { createStaffApi } from "@services/api/staff"

export interface HookInterface {
  failureCallback?: (err: AxiosError | any) => void
  successCallback?: () => void
}

type ReturnDataType = {
  isLoading: boolean
  errors: AxiosError
  runRequest: (data: AddStaff) => void
}

export const useCreateStaff: (args: HookInterface) => ReturnDataType = ({
  failureCallback: componentFailureCallback,
  successCallback: componentSuccessCallback,
}) => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<AxiosError | any>()

  const runRequest = async (data: AddStaff) => {
    try {
      setLoading(true)
      const response = await createStaffApi(data)
      if (response.data) {
        setLoading(false)
        componentSuccessCallback && componentSuccessCallback()
      }
    } catch (error) {
      setLoading(false)
      setErrors(error)
      componentFailureCallback && componentFailureCallback(error)
    }
  }

  const returnObject: ReturnDataType = {
    isLoading,
    errors,
    runRequest,
  }

  return returnObject
}
