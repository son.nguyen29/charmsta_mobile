import { useState } from "react"
import { AxiosError } from "axios"

import { StaffByServiceDTO } from "@models/response/Staff"
import { getStaffByServiceApi, ParamsStaffService } from "@services/api/staff"

export interface HookInterface {
  failureCallback?: (err: AxiosError | any) => void
  successCallback?: () => void
}

type ReturnDataType = {
  isLoading: boolean
  errors: AxiosError
  data?: StaffByServiceDTO[]
  runRequest: (serviceId: number, params?: ParamsStaffService) => void
}

export const usStaffService: (args: HookInterface) => ReturnDataType = ({
  failureCallback: componentFailureCallback,
  successCallback: componentSuccessCallback,
}) => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<AxiosError | any>()
  const [data, setData] = useState<StaffByServiceDTO[]>()

  const runRequest = async (serviceId: number, params?: ParamsStaffService) => {
    try {
      setLoading(true)
      const response = await getStaffByServiceApi(serviceId, params)
      if (response.data) {
        setLoading(false)
        setData(response.data)
        componentSuccessCallback && componentSuccessCallback()
      }
    } catch (error) {
      setLoading(false)
      setErrors(error)
      componentFailureCallback && componentFailureCallback(error)
    }
  }

  const returnObject: ReturnDataType = {
    isLoading,
    data,
    errors,
    runRequest,
  }

  return returnObject
}
