import { useState, useEffect } from "react"
import { AxiosError } from "axios"

import { PaginationStaffDTO } from "@models/response/Staff"
import { getStaffApi } from "@services/api/staff"

export interface HookInterface {
  failureCallback?: (err: AxiosError | any) => void
  successCallback?: () => void
}

type ReturnDataType = {
  isLoading: boolean
  errors: AxiosError
  data?: PaginationStaffDTO
  runRequest: () => void
}

export const useStaff: (args: HookInterface) => ReturnDataType = ({
  failureCallback: componentFailureCallback,
  successCallback: componentSuccessCallback,
}) => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<AxiosError | any>()
  const [data, setData] = useState<PaginationStaffDTO>()
  const [page, setPage] = useState<number>(0)
  const [size, setSize] = useState<number>(20)

  const runRequest = async () => {
    try {
      setLoading(true)
      const response = await getStaffApi({ page, size })
      if (response.data) {
        setLoading(false)
        setData(response.data)
        componentSuccessCallback && componentSuccessCallback()
      }
    } catch (error) {
      setLoading(false)
      setErrors(error)
      componentFailureCallback && componentFailureCallback(error)
    }
  }

  useEffect(() => {
    runRequest()
  }, [])

  const returnObject: ReturnDataType = {
    isLoading,
    data,
    errors,
    runRequest,
  }

  return returnObject
}
