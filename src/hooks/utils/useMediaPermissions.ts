import { useState, useCallback } from "react"
import { Alert, Platform } from "react-native"
import { useFocusEffect } from "@react-navigation/native"
import {
  check,
  request,
  PERMISSIONS,
  RESULTS,
  openSettings,
} from "react-native-permissions"

const CAMERA_PERMISSION =
  Platform.OS === "ios" ? PERMISSIONS.IOS.CAMERA : PERMISSIONS.ANDROID.CAMERA

const PHOTOS_PERMISSION =
  Platform.OS === "ios"
    ? PERMISSIONS.IOS.PHOTO_LIBRARY
    : PERMISSIONS.ANDROID.READ_MEDIA_IMAGES

export const useMediaPermissions = () => {
  const [isCameraGranted, setCameraGranted] = useState(false)
  const [isPhotosGranted, setPhotosGranted] = useState(false)

  const isGranted = isCameraGranted && isPhotosGranted

  const showAlert = () => {
    Alert.alert(
      "",
      "App requires to access Camera/Photos for uploading photos to your profile",
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        { text: "Open settings", onPress: () => openSettings() },
      ],
    )
  }

  const requestCameraPermission = () => {
    request(CAMERA_PERMISSION).then((result) => {
      switch (result) {
        case RESULTS.GRANTED:
          setCameraGranted(true)
          break
        case RESULTS.BLOCKED:
          showAlert()
          break
        default:
          break
      }
    })
  }

  const requestPhotoPermission = () => {
    request(PHOTOS_PERMISSION).then((result) => {
      switch (result) {
        case RESULTS.GRANTED:
          setPhotosGranted(true)
          break
        case RESULTS.BLOCKED:
          showAlert()
          break
        default:
          break
      }
    })
  }

  const checkCameraPermission = () => {
    check(CAMERA_PERMISSION).then((result) => {
      switch (result) {
        case RESULTS.GRANTED:
          setCameraGranted(true)
          break
        case RESULTS.BLOCKED:
          showAlert()
          break
        default:
          requestCameraPermission()
          break
      }
    })
  }

  const checkPhotoPermission = () => {
    check(PHOTOS_PERMISSION).then((result) => {
      switch (result) {
        case RESULTS.GRANTED:
          setPhotosGranted(true)
          break
        case RESULTS.BLOCKED:
          showAlert()
          break
        default:
          requestPhotoPermission()
          break
      }
    })
  }

  useFocusEffect(
    useCallback(() => {
      checkCameraPermission()
      checkPhotoPermission()
    }, []),
  )

  const requestPermissions = () => {
    requestCameraPermission()
    requestPhotoPermission()
  }

  return { isGranted, requestPermissions }
}
