import { useState } from "react"
import { PermissionsAndroid, Platform } from "react-native"
import RNContacts from "react-native-contacts"

interface ReturnDataType {
  requestPermissions: () => void
  isGranted: boolean
}

export const useContactPermissions: () => ReturnDataType = () => {
  const [iosGranted, setIosGranted] = useState(false)
  const [androidGranted, setAndroidGranted] = useState(false)

  const isGranted = iosGranted || androidGranted

  const requestPermissions = () => {
    if (Platform.OS == "ios") {
      RNContacts.checkPermission().then((response) => {
        switch (response) {
          case "authorized":
            setIosGranted(true)
            break
          case "denied":
            break
          case "undefined":
            RNContacts.requestPermission().then((res) => {
              switch (res) {
                case "authorized":
                  setIosGranted(true)
                  break
              }
            })
            break
        }
      })
    }
    if (Platform.OS == "android") {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: "Contacts",
        message: "Charmsta would like to view your contacts.",
        buttonPositive: "Please accept bare mortal",
      }).then((res) => {
        switch (res) {
          case "granted":
            setAndroidGranted(true)
            break
        }
      })
    }
  }

  return { isGranted, requestPermissions }
}
