import { UploadDTO } from "@models/response/Utils"
import { uploadApi } from "@services/api/utils"
import { AxiosError } from "axios"
import { isEmpty, isFunction } from "lodash"
import { useState } from "react"
import { Image } from "react-native-image-crop-picker"

interface ReturnDataType {
  isLoading: boolean
  error: AxiosError
  runRequest: (images: Image[]) => void
}

interface HookInterface {
  successCallback?: (
    uploads: Array<UploadDTO & { belongedToIndex: number }>,
  ) => void
  failureCallback?: (error: AxiosError | any) => void
}

export const useUploadImages: (args: HookInterface) => ReturnDataType = ({
  failureCallback,
  successCallback,
}) => {
  const [error, setError] = useState<AxiosError | any>()
  const [isLoading, setLoading] = useState<boolean>(false)

  const runRequest = async (images: Image[]) => {
    try {
      setLoading(true)
      const queue = images.map((image) => {
        if (!isEmpty(image.path)) {
          const formData = new FormData()
          formData.append("file", {
            uri: image.path,
            type: image.mime || "image/jpeg",
            name: image.path.substring(image.path.lastIndexOf("/" + 1)),
          } as unknown as Blob)
          return uploadApi(formData)
        } else {
          return null
        }
      })

      const res = await Promise.all(queue)

      if (res.length > 0) {
        isFunction(successCallback) &&
          successCallback(
            res.map((i) =>
              isEmpty(i)
                ? null
                : {
                    ...i.data,
                    belongedToIndex: images.findIndex((photo) =>
                      photo?.path?.includes(i.data.originalname),
                    ),
                  },
            ),
          )
      }
      setLoading(false)
    } catch (err) {
      setError(err)
      setLoading(false)
      isFunction(failureCallback) && failureCallback(err)
    }
  }

  return {
    isLoading,
    error,
    runRequest,
  }
}
