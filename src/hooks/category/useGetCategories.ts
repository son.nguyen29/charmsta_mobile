import { CategoryDTO } from "@models/response/Service"
import { getCategoriestApi } from "@services/api/category"
import { AxiosError } from "axios"
import { isFunction } from "lodash"
import { useState } from "react"

interface ReturnDataType {
  isLoading: boolean
  error: AxiosError
  runRequest: (params?: {
    search?: string
  }) => void
}

interface HookInterface {
  successCallback?: (customers: CategoryDTO[]) => void
  failureCallback?: (error: AxiosError | any) => void
}

export const useGetCategories: (args: HookInterface) => ReturnDataType = ({
  failureCallback,
  successCallback,
}) => {
  const [isLoading, setLoading] = useState(false)
  const [error, setError] = useState<AxiosError | any>()

  const runRequest = async (params: { search: string } = { search: "" }) => {
    try {
      setLoading(true)
      const response = await getCategoriestApi(params)
      if (response?.data) {
        isFunction(successCallback) && successCallback(response?.data)
        setLoading(false)
      }
    } catch (err) {
      setError(err)
      isFunction(failureCallback) && failureCallback(err)
      setLoading(false)
    }
  }

  return { isLoading, error, runRequest }
}
