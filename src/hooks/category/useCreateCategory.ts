import { useState } from "react"
import { AxiosError } from "axios"

import { AddCategory } from "@models/request/Category"
import { addCategoryApi } from "@services/api/category"

export interface HookInterface {
  failureCallback?: (err: AxiosError | any) => void
  successCallback?: () => void
}

type ReturnDataType = {
  isLoading: boolean
  errors: AxiosError
  runRequest: (params: AddCategory) => void
}

export const useCreateCategory: (args: HookInterface) => ReturnDataType = ({
  failureCallback: componentFailureCallback,
  successCallback: componentSuccessCallback,
}) => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<AxiosError | any>()

  const runRequest = async (data: AddCategory) => {
    try {
      setLoading(true)
      const response = await addCategoryApi(data)
      if (response.data) {
        setLoading(false)
        componentSuccessCallback && componentSuccessCallback()
      }
    } catch (error) {
      setLoading(false)
      setErrors(error)
      componentFailureCallback && componentFailureCallback(error)
    }
  }

  const returnObject: ReturnDataType = {
    isLoading,
    errors,
    runRequest,
  }

  return returnObject
}
