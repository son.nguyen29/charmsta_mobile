import { useState, useEffect } from "react"
import { AxiosError } from "axios"

import { CategoryDTO } from "@models/response/Category"
import { getCategoriesApi } from "@services/api/category"

export interface HookInterface {
  failureCallback?: (err: AxiosError | any) => void
  successCallback?: () => void
}

type ReturnDataType = {
  isLoading: boolean
  errors: AxiosError
  data: CategoryDTO[]
  runRequest: () => void
}

export const useCategory: (args: HookInterface) => ReturnDataType = ({
  failureCallback: componentFailureCallback,
  successCallback: componentSuccessCallback,
}) => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<AxiosError | any>()
  const [data, setData] = useState<CategoryDTO[]>([])

  const runRequest = async () => {
    try {
      setLoading(true)
      const response = await getCategoriesApi()
      if (response.data) {
        setLoading(false)
        setData(response.data)
        componentSuccessCallback && componentSuccessCallback()
      }
    } catch (error) {
      setLoading(false)
      setErrors(error)
      componentFailureCallback && componentFailureCallback(error)
    }
  }

  useEffect(() => {
    runRequest()
  }, [])

  const returnObject: ReturnDataType = {
    isLoading,
    data,
    errors,
    runRequest,
  }

  return returnObject
}
