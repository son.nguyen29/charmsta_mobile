import { useState } from "react"

import { getStoresPrediction } from "@services/api/store"
import { IStoreRegister } from "@models/store/StoreStore"

export const useStore = () => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [storesPrediction, setStoresPrediction] = useState<IStoreRegister[]>([])

  const searchStores = async (
    input: string,
    type?: "establishment" | "geocode",
  ) => {
    try {
      setLoading(true)
      const response = await getStoresPrediction(input, type)
      if (response.data?.predictions) {
        setStoresPrediction(response.data.predictions)
      }
    } catch (error) {
    } finally {
      setLoading(false)
    }
  }

  return {
    isLoading,
    storesPrediction,
    searchStores,
  }
}
