import { useState } from "react"
import { AxiosError } from "axios"

import { getPlaceDetails } from "@services/api/store"
import { useStores } from "@models/index"

export interface HookInterface {
  failureCallback?: (err: AxiosError | any) => void
  successCallback?: () => void
}

type ReturnDataType = {
  isLoading: boolean
  errors: AxiosError
  runRequest: (placeid: string) => void
}

export const useSearchZipcode: (args: HookInterface) => ReturnDataType = ({
  failureCallback: componentFailureCallback,
  successCallback: componentSuccessCallback,
}) => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<AxiosError | any>()
  const {
    storeStore: { setZipcode },
  } = useStores()

  const runRequest = async (placeid: string) => {
    try {
      setLoading(true)
      const response = await getPlaceDetails(placeid)
      if (response.data?.result?.address_components) {
        const item = response.data.result.address_components.find((item) =>
          item.types.includes("postal_code"),
        )
        setZipcode(item?.long_name || "")
        componentSuccessCallback && componentSuccessCallback()
      } else {
        componentSuccessCallback && componentSuccessCallback()
      }
    } catch (error) {
      setErrors(error)
      componentFailureCallback && componentFailureCallback(error)
    } finally {
      setLoading(false)
    }
  }

  const returnObject: ReturnDataType = {
    isLoading,
    errors,
    runRequest,
  }

  return returnObject
}
