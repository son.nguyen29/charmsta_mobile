import { useState } from "react"
import { AxiosError } from "axios"

import { getStoresPrediction } from "@services/api/store"
import { IStoreRegister } from "@models/store/StoreStore"

export interface HookInterface {
  failureCallback?: (err: AxiosError | any) => void
  successCallback?: () => void
}

export type SearchType = "establishment" | "geocode"

type ReturnDataType = {
  isLoading: boolean
  errors: AxiosError
  data: IStoreRegister[]
  runRequest: (input: string, type?: SearchType) => void
}

export const useSearchStores: (args: HookInterface) => ReturnDataType = ({
  failureCallback: componentFailureCallback,
  successCallback: componentSuccessCallback,
}) => {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [errors, setErrors] = useState<AxiosError | any>()
  const [data, setData] = useState<IStoreRegister[]>([])

  const runRequest = async (input: string, type?: SearchType) => {
    try {
      setLoading(true)
      const response = await getStoresPrediction(input, type)
      if (response.data?.predictions) {
        setData(response.data.predictions)
        componentSuccessCallback && componentSuccessCallback()
      }
    } catch (error) {
      setErrors(error)
      componentFailureCallback && componentFailureCallback(error)
    } finally {
      setLoading(false)
    }
  }

  const returnObject: ReturnDataType = {
    isLoading,
    errors,
    data,
    runRequest,
  }

  return returnObject
}
