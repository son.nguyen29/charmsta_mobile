import { RFValue } from "d4dpocket"

export const fontSize = {
  fontSize12: RFValue(12),
  fontSize14: RFValue(14),
  fontSize16: RFValue(16),
  fontSize18: RFValue(18),
  fontSize22: RFValue(22),
  fontSize26: RFValue(26),
} as const

export type FontSize = keyof typeof fontSize
