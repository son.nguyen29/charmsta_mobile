import { types } from "mobx-state-tree"

export const UserRegisterModel = types.model("UserRegister").props({
  fullName: types.maybe(types.string),
  email: types.maybe(types.string),
  password: types.maybe(types.string),
})

export const AuthModel = types.model("Auth").props({
  refreshToken: types.maybe(types.string),
  token: types.maybe(types.string),
  onboarding: types.optional(types.boolean, true),
})
