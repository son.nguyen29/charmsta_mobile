import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { AuthModel, UserRegisterModel } from "./AuthModel"
import { saveString, remove } from "@utils/storage"

export const AuthStoreModel = types
  .model("AuthStore")
  .props({
    Auth: types.optional(AuthModel, {}),
    userRegister: types.optional(UserRegisterModel, {}),
  })
  .views((self) => ({
    get isAuthenticated() {
      return !!self.Auth.token
    },
    get isOnboarding() {
      return self.Auth.onboarding
    },
  }))
  .actions((self) => ({
    setAuthToken(value?: string) {
      if (value) {
        self.Auth.token = value
        saveString("@token", value)
      }
    },
    setOnboarding(value: boolean) {
      self.Auth.onboarding = value
    },
    logout() {
      self.Auth.token = undefined
      remove("@token")
    },
    setUserRegister(value: Instance<typeof UserRegisterModel>) {
      self.userRegister = value
    },
  }))

export type AuthStore = Instance<typeof AuthStoreModel>
export type AuthStoreSnapshot = SnapshotOut<typeof AuthStoreModel>
