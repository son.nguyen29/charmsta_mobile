import { types } from "mobx-state-tree"

export const StoreRegister = types.model({
  description: types.string,
  place_id: types.string,
  reference: types.string,
  structured_formatting: types.model({
    main_text: types.string,
    secondary_text: types.string,
  }),
  terms: types.array(
    types.model({
      offset: types.number,
      value: types.string,
    }),
  ),
  types: types.array(types.string),
})

export const StoreModel = types.model("Store").props({
  storeRegister: types.maybeNull(StoreRegister),
})
