import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { StoreRegister } from "./StoreModel"

export const StoreStoreModel = types
  .model("StoreStore")
  .props({
    storeRegister: types.maybeNull(StoreRegister),
    zipcode: types.maybeNull(types.string),
  })
  .actions((self) => ({
    setStoreRegister(value: Instance<typeof StoreRegister>) {
      self.storeRegister = value
    },
    setZipcode(value: string) {
      self.zipcode = value
    },
  }))

export type IStoreRegister = Instance<typeof StoreRegister>
export type StoreStore = Instance<typeof StoreStoreModel>
export type StoreStoreSnapshot = SnapshotOut<typeof StoreStoreModel>
