export interface ServiceDTO {
  id: number
  name: string
  cost: number
  price: number
  stocks: number
  description?: string
  photo?: string
  thumb?: string
  color?: string
  orderBy: number
  serviceDuration: number
  SKU?: string
  storeId: number
  suppilerId?: number
  categoryId?: number
  staffId: number
  // staffs: StaffDTO[]
  // staff: StaffDTO
  isActive: boolean
  // tax?: TaxDTO
}
