export interface PredictionTermsDTO {
  offset: number
  value: string
}

export interface StorePredictionDTO {
  description: string
  place_id: string
  reference: string
  structured_formatting: {
    main_text: string
    secondary_text: string
  }
  terms: PredictionTermsDTO[]
  types: string[]
}

export interface PlaceAddressComponentsDTO {
  long_name: string
  short_name: string
  types: string[]
}

export type PlacesAutocompleteStatus =
  | "OK"
  | "ZERO_RESULTS"
  | "INVALID_REQUEST"
  | "OVER_QUERY_LIMIT"
  | "REQUEST_DENIED"
  | "UNKNOWN_ERROR"

export interface PlaceDetailsDTO {
  html_attributions: any[]
  result: {
    address_components: PlaceAddressComponentsDTO[]
  }
  name: string
  status: PlacesAutocompleteStatus
}
