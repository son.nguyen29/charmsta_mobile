export interface CustomerDTO {
  id: number
  avatar: string
  firstName: string
  lastName: string
  phoneNumber: string
  email: string
  dob: string
  address: string
  city: string
  state: string
  zipcode: string
}
