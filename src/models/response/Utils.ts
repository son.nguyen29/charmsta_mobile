export interface UploadDTO {
  filename: string
  mimetype: string
  originalname: string
  size: number
  thumb: string
  url: string
}
