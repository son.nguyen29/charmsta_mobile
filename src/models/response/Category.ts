import { ServiceDTO } from "./Service"

export interface CategoryDTO {
    id: number,
    name: string
    services: ServiceDTO[]
}
