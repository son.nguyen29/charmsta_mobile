export interface CreateCustomer {
  firstName: string
  lastName: string
  phoneNumber: string
  email: string
  dob: string
  address: string
  city: string
  state: string
  zipcode: string
  avatar: string
}

export type ImportCustomer = CreateCustomer
