export interface AddStaff {
  avatar?: string
  name: string
  phoneNumber: string
  email: string
  break?: Array<{ id: number }>
  workingDay?: Array<{ id: number }>
  permission: number
}
