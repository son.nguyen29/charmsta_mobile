import { StaffDTO } from "@models/response/Staff"

export interface AddService {
  name: string
  serviceDuration: string
  cost: string
  category: number
  staff: StaffDTO[]
}
