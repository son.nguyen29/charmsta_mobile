import { FirebaseAuthTypes } from "@react-native-firebase/auth"
import { StoreRegister } from "./Store"

export interface LoginEmail {
  email: string
  password: string
  deviceToken?: string
}

export interface UserRegister {
  fullName: string
  email: string
  password: string
}

export interface SignUpEmail extends UserRegister {
  store: StoreRegister
  deviceToken: string
}

export interface ForgotPassword {
  email: string
}

export interface SocialSignIn extends FirebaseAuthTypes.User {
  deviceToken: string
}
