import { ServiceDTO } from "../response/Service"

export interface AddCategory {
  name: string
  services: ServiceDTO[]
}