export interface StoreCategory {
  id: number
  name: string
}

export interface StoreRegister {
  name: string
  address: string
  city: string
  state: string
  zipcode: string
  categories: StoreCategory[]
  phoneNumber: string
}
