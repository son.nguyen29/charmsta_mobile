import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { AuthStoreModel } from "./store/AuthStore"
import { StoreStoreModel } from "./store/StoreStore"

/**
 * A RootStore model.
 */
export const RootStoreModel = types.model("RootStore").props({
  authStore: types.optional(AuthStoreModel, {}),
  storeStore: types.optional(StoreStoreModel, {}),
})

/**
 * The RootStore instance.
 */
export type RootStore = Instance<typeof RootStoreModel>
/**
 * The data of a RootStore.
 */
export type RootStoreSnapshot = SnapshotOut<typeof RootStoreModel>
