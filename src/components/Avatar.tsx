import { colors } from "@theme/colors"
import * as React from "react"
import {
  ActivityIndicator,
  Image,
  ImageStyle,
  StyleSheet,
  View,
} from "react-native"

interface AvatarProps {
  uri?: string
  imageStyle?: ImageStyle
  size?: number
  loading?: boolean
}

const Avatar = (props: AvatarProps) => {
  const { uri, imageStyle = {}, size, loading = false } = props
  const customSize = { height: size || 120, width: size || 120 }

  return (
    <View style={[styles.container, customSize]}>
      {loading && (
        <ActivityIndicator
          animating={true}
          style={[styles.indicator, customSize]}
        />
      )}
      <Image
        source={{ uri: uri || undefined }}
        style={[styles.image, imageStyle, customSize]}
      />
    </View>
  )
}

export default Avatar

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    borderRadius: 500,
    backgroundColor: colors.palette.secondary200,
  },
  indicator: {
    zIndex: 1,
    position: "absolute",
    borderRadius: 500,
  },
})
