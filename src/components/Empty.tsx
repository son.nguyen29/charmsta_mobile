import React from "react"
import { View, StyleSheet, ViewProps } from "react-native"

import { Text } from "./basic/Text"

const Empty = (props: ViewProps) => {
  return (
    <View style={styles.container} {...props}>
      <Text>Empty</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
})

export { Empty }
