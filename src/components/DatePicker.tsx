import { isFunction } from "lodash"
import { Ref } from "react"
import { StyleSheet } from "react-native"
import DatePicker from "react-native-datepicker"

export interface DatePickerModalRef {
  setModalVisible: (visible: boolean) => void
}

interface DatePickerModalProps {
  format?: string
  datePickerRef?: Ref<DatePickerModalRef>
  mode?: "date" | "datetime" | "time"
  placeholder?: string
  confirmBtnText?: string
  cancelBtnText?: string
  onDateChange?: (date: string) => void
}

export const DatePickerModal = (props: DatePickerModalProps) => {
  const {
    format = "YYYY-MM-DD",
    datePickerRef,
    mode = "date",
    placeholder = "select date",
    confirmBtnText = "Confirm",
    cancelBtnText = "Cancel",
    onDateChange,
  } = props
  return (
    <DatePicker
      ref={datePickerRef}
      hideText={true}
      showIcon={false}
      date={new Date()}
      mode={mode}
      placeholder={placeholder}
      format={format}
      confirmBtnText={confirmBtnText}
      cancelBtnText={cancelBtnText}
      customStyles={{ dateTouchBody: styles.dateTouchBody }}
      onDateChange={(date: string) =>
        isFunction(onDateChange) && onDateChange(date)
      }
    />
  )
}

const styles = StyleSheet.create({
  container: {},
  dateTouchBody: { height: 0 },
})
