import { Button, scale } from "d4dpocket"
import { FormikErrors, FormikTouched } from "formik"
import React, { forwardRef, useState } from "react"
import {
  Pressable,
  StyleSheet,
  TextInput,
  TextInputProps,
  View,
  ViewStyle,
} from "react-native"
import Feather from "react-native-vector-icons/Feather"

import { fontSize } from "@theme/fontSize"
import { isFunction } from "lodash"
import { Text } from "./basic/Text"

type FormikValues = {
  [field: string]: any
}

export type FieldProps = {
  onPress?: () => void
  label?: string
  style?: ViewStyle
  name: string
  inputStyle?: ViewStyle
  styleError?: ViewStyle
  required?: boolean
  isPassword?: boolean
  errors?: FormikErrors<FormikValues>
  touched?: FormikTouched<FormikValues>
  autoCapitalize?: "none" | "sentences" | "words" | "characters" | undefined
  handleChange<T_1 = string | React.ChangeEvent<any>>(
    field: T_1,
  ): T_1 extends React.ChangeEvent<any>
    ? void
    : (e: string | React.ChangeEvent<any>) => void
  handleBlur<T = string | any>(
    fieldOrEvent: T,
  ): T extends string ? (e: any) => void : void
} & TextInputProps

export const Field = forwardRef(
  (
    {
      label,
      style,
      inputStyle,
      styleError,
      name,
      required,
      isPassword,
      errors,
      touched,
      handleChange,
      handleBlur,
      autoCapitalize,
      onPress,
      ...rest
    }: FieldProps,
    ref: React.Ref<TextInput>,
  ) => {
    const [secureTextEntry, setSecureTextEntry] = useState(isPassword)

    const changeSecureTextEntry = () => {
      setSecureTextEntry((prev) => !prev)
    }

    return (
      <Pressable disabled={!isFunction(onPress)} onPress={onPress}>
        <View {...{ style }}>
          {label && (
            <Text style={styles.label}>
              {label}
              {required && <Text style={styles.required}>*</Text>}
            </Text>
          )}
          <View>
            <TextInput
              ref={ref}
              {...rest}
              secureTextEntry={secureTextEntry}
              style={[styles.input, inputStyle]}
              onChangeText={handleChange(name)}
              onBlur={handleBlur(name)}
              autoCapitalize={autoCapitalize || "none"}
              pointerEvents={isFunction(onPress) ? "none" : "auto"}
            />
            {isPassword && (
              <Button onPress={changeSecureTextEntry}>
                <Feather
                  size={fontSize.fontSize16}
                  name={secureTextEntry ? "eye-off" : "eye"}
                />
              </Button>
            )}
          </View>
          {touched?.[name] && errors?.[name] ? (
            <Text style={[styles.error, styleError]}>
              {errors[name] as string}
            </Text>
          ) : null}
        </View>
      </Pressable>
    )
  },
)

const styles = StyleSheet.create({
  required: {
    color: "#FF0000",
  },
  error: {
    color: "#FF0000",
    fontSize: fontSize.fontSize12,
    marginTop: scale(5),
  },
  label: {
    marginBottom: scale(5),
  },
  input: {
    fontSize: fontSize.fontSize16,
  },
})
