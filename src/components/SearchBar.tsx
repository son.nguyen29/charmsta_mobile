import React, { useCallback, useState } from "react"
import {
  StyleSheet,
  TextInput,
  TextInputProps,
  View,
  ViewStyle,
} from "react-native"
import debounce from "lodash/debounce"
import AntDesign from "react-native-vector-icons/AntDesign"
import { RFValue, Button } from "d4dpocket"

interface SearchBarProps extends TextInputProps {
  styleContainer?: ViewStyle
  onSubmit?: (text: string) => void
}

const SearchBar = ({
  styleContainer,
  placeholder,
  onSubmit,
  ...rest
}: SearchBarProps) => {
  const [value, setValue] = useState<string>("")

  const handleSearch = useCallback(
    debounce((text) => {
      onSubmit?.(text)
    }, 500),
    [],
  )

  const onChangeText = (text: string) => {
    setValue(text)
    handleSearch(text)
  }

  const onRemove = () => {
    setValue("")
    onSubmit?.("")
  }

  return (
    <View style={[styles.container, styleContainer]}>
      <TextInput
        {...rest}
        value={value}
        placeholder={placeholder || "Enter a phrase here"}
        onChangeText={onChangeText}
      />
      <Button onPress={onRemove}>
        <AntDesign name="close" color="#000" size={RFValue(16)} />
      </Button>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {},
})

export { SearchBar }
