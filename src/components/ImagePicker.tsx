import { Button, scale } from "d4dpocket"
import React, { useState } from "react"
import { StyleSheet, Text, View } from "react-native"
import { Image, openCamera, openPicker } from "react-native-image-crop-picker"
import Modal from "react-native-modal"

import { useMediaPermissions } from "@hooks/utils/useMediaPermissions"
import { colors, fontSize } from "@theme/index"
import { isFunction } from "lodash"

interface ImagePickerProps {
  children: React.ReactNode
  onSelect?: (photo: Image) => void
  onClose?: () => void
}

const ImagePicker = ({ children, onSelect, onClose }: ImagePickerProps) => {
  const [isVisible, setVisible] = useState(false)
  const { isGranted, requestPermissions } = useMediaPermissions()

  const handleOpenCamera = () => {
    closeModal(() => {
      openCamera({
        mediaType: "photo",
        width: 1000,
        height: 1000,
        cropping: true,
      }).then((photo) => {
        console.log("openCamera", photo)
        isFunction(onSelect) && onSelect(photo)
      })
    })
  }

  const handleOpenPicker = () => {
    closeModal(() => {
      openPicker({
        mediaType: "photo",
      }).then((photo) => {
        console.log("openPicker", photo)
        isFunction(onSelect) && onSelect(photo)
      })
    })
  }

  const openModal = () => {
    if (isGranted) {
      setVisible(true)
    } else {
      requestPermissions()
    }
  }

  const closeModal = (cb?: () => void) => {
    setVisible(false)
    isFunction(onClose) && onClose()
    setTimeout(() => {
      cb && cb()
    }, 450)
  }

  return (
    <View>
      <Button onPress={openModal}>{children}</Button>
      <Modal
        useNativeDriver
        isVisible={isVisible}
        backdropTransitionOutTiming={0}
        hideModalContentWhileAnimating
        onBackButtonPress={closeModal}
        onBackdropPress={closeModal}
        style={styles.modal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.content}>
            <Button
              onPress={handleOpenCamera}
              style={[styles.btn, styles.btnCamera]}
            >
              <Text style={styles.text}>Camera</Text>
            </Button>
            <Button onPress={handleOpenPicker} style={styles.btn}>
              <Text style={styles.text}>Photos</Text>
            </Button>
          </View>
          <Button onPress={() => closeModal()} style={styles.btnCancel}>
            <Text style={[styles.text, styles.textCancel]}>Cancel</Text>
          </Button>
        </View>
      </Modal>
    </View>
  )
}

const styles = StyleSheet.create({
  modal: {
    marginVertical: scale(10),
    marginHorizontal: scale(20),
    justifyContent: "flex-end",
  },
  modalContainer: {
    maxHeight: "90%",
  },
  content: {
    backgroundColor: "#FFFFFF",
    borderRadius: scale(12),
    marginBottom: scale(6),
  },
  btn: {
    alignItems: "center",
    paddingVertical: scale(6),
  },
  btnCamera: {
    borderBottomColor: colors.separator,
    borderBottomWidth: 1,
  },
  text: {
    fontSize: fontSize.fontSize16,
  },
  btnCancel: {
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: scale(12),
    paddingVertical: scale(6),
  },
  textCancel: {
    color: "#ff0000",
  },
})

export { ImagePicker }
