import { Button, scale } from "d4dpocket"
import React, { useState } from "react"
import { FlatList, StyleSheet, View, ViewProps } from "react-native"
import Modal from "react-native-modal"
import MCIcons from "react-native-vector-icons/MaterialCommunityIcons"

import { colors, fontSize } from "@theme/index"
import { Text } from "./basic/Text"

type Item<ItemT> = {
  [K in keyof ItemT]?: ItemT[K]
} & { id: number }

interface IPickerProps<ItemT> extends ViewProps {
  titleModal?: string
  data?: Item<ItemT>[]
  value?: Item<ItemT>
  getItemLabel?: (item: Item<ItemT>) => string
  onSelect?: (item: Item<ItemT>) => void
  children: React.ReactNode
}

const Picker = <ItemT,>({
  testID,
  titleModal,
  data,
  value,
  getItemLabel,
  onSelect,
  children,
}: IPickerProps<ItemT>) => {
  const [isVisible, setVisible] = useState<boolean>(false)

  const openModal = () => {
    setVisible(true)
  }

  const closeModal = () => {
    setVisible(false)
  }

  const handleSelect = (item: Item<ItemT>) => {
    closeModal()
    onSelect && onSelect(item)
  }

  const keyExtractor = (_: Item<ItemT>, index: number) => index?.toString()

  const renderItem = ({ item }) => {
    const isSelected = item?.id === value?.id
    return (
      <Button style={styles.item} onPress={() => handleSelect(item)}>
        <Text>{getItemLabel ? getItemLabel?.(item) : item?.name}</Text>
        <MCIcons
          size={fontSize.fontSize16}
          name={
            isSelected
              ? "checkbox-marked-circle"
              : "checkbox-blank-circle-outline"
          }
        />
      </Button>
    )
  }

  return (
    <View testID={testID}>
      <Button onPress={openModal}>{children}</Button>
      <Modal
        useNativeDriver
        isVisible={isVisible}
        backdropTransitionOutTiming={0}
        hideModalContentWhileAnimating
        onBackButtonPress={closeModal}
        onBackdropPress={closeModal}
        style={styles.modal}
      >
        <View style={styles.modalContainer}>
          <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={keyExtractor}
            style={styles.content}
            showsVerticalScrollIndicator={false}
            stickyHeaderIndices={[0]}
            ListHeaderComponent={
              titleModal ? (
                <View style={styles.header}>
                  <Text style={styles.title}>{titleModal}</Text>
                </View>
              ) : null
            }
          />
          <Button onPress={closeModal} style={styles.btnCancel}>
            <Text>Cancel</Text>
          </Button>
        </View>
      </Modal>
    </View>
  )
}

const styles = StyleSheet.create({
  modal: {
    marginVertical: scale(10),
    marginHorizontal: scale(20),
    justifyContent: "flex-end",
  },
  modalContainer: {
    maxHeight: "90%",
  },
  content: {
    backgroundColor: colors.palette.neutral100,
    borderRadius: scale(12),
  },
  item: {
    paddingVertical: scale(6),
    paddingHorizontal: scale(12),
    borderTopColor: "#F2F2F2",
    borderTopWidth: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  btnCancel: {
    backgroundColor: colors.palette.neutral100,
    borderRadius: scale(12),
    paddingVertical: scale(6),
    marginTop: scale(8),
    alignItems: "center",
  },
  header: {
    backgroundColor: colors.palette.neutral100,
    paddingTop: scale(6),
    paddingBottom: scale(4),
    paddingHorizontal: scale(12),
  },
  title: {
    fontWeight: "700",
  },
})

export { Picker }
export type { IPickerProps }
