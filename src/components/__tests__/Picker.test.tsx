import React from "react"
import { Text } from "react-native"
import { render } from "@testing-library/react-native"

import { Picker } from "../Picker"

const data = [
  { id: 1, name: "Name 1" },
  { id: 2, name: "Name 2" },
  { id: 3, name: "Name 3" },
]
const pickerProps = {
  testID: "picker-id",
  titleModal: "Title modal",
  data,
  value: "",
  onSelect: jest.fn(),
}

const TestPicker = () => (
  <Picker
    testID={pickerProps.testID}
    data={pickerProps.data}
    titleModal={pickerProps.titleModal}
  >
    <Text>Test Picker</Text>
  </Picker>
)

describe("Picker", () => {
  test("rendered", async () => {
    const { findByTestId } = render(<TestPicker />)
    const component = await findByTestId(pickerProps.testID)
    expect(component).toBeTruthy()
  })
})
