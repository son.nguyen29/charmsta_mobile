import React from "react"
import { render } from "@testing-library/react-native"

import { Empty } from "../Empty"

const EmptyID = "empty-id"

describe("Empty", () => {
  test("should render the component", async () => {
    const { findByTestId } = render(<Empty testID={EmptyID} />)
    const component = await findByTestId(EmptyID)
    expect(component).toBeTruthy()
  })
})
