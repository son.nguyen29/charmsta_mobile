import { colors } from "@theme/colors"
import { isFunction } from "lodash"
import React from "react"
import { Pressable, StyleSheet, Text, ViewStyle } from "react-native"

interface FloatButtonProps {
  size?: number
  onPress?: () => void
  content?: string
  style?: ViewStyle
}

const getInitialSize = (size: number) => ({
  width: size || 32,
  height: size || 32,
  borderRadius: size || 32,
})

const FloatButton = (props: FloatButtonProps) => {
  const size = getInitialSize(props.size)

  const _onPress = () => {
    isFunction(props.onPress) && props.onPress()
  }

  return (
    <Pressable style={[styles.container, props.style, size]} onPress={_onPress}>
      <Text style={[styles.content]}>{props.content}</Text>
    </Pressable>
  )
}

export default FloatButton

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    bottom: 16,
    right: 16,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.palette.neutral700,
  },
  content: { fontSize: 32, color: colors.palette.neutral100 },
})
