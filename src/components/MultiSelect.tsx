import React, { useState } from "react"
import { StyleSheet, View, FlatList } from "react-native"
import { Button, scale } from "d4dpocket"
import Modal from "react-native-modal"
import MCIcons from "react-native-vector-icons/MaterialCommunityIcons"

import { Text } from "./basic/Text"
import { colors, fontSize } from "@theme/index"

type Item<ItemT> = {
  [K in keyof ItemT]?: ItemT[K]
} & { id: number }

interface IMultiSelectProps<ItemT> {
  titleModal?: string
  data?: Item<ItemT>[]
  value?: Item<ItemT>[]
  getItemLabel?: (item: Item<ItemT>) => string
  onSubmit?: (item: Item<ItemT>[]) => void
  children: React.ReactNode
}

const MultiSelect = <ItemT,>({
  titleModal,
  data,
  value,
  getItemLabel,
  onSubmit,
  children,
}: IMultiSelectProps<ItemT>) => {
  const [isVisible, setVisible] = useState<boolean>(false)
  const [dataSelected, setDataSelected] = useState<Item<ItemT>[]>(value || [])

  const openModal = () => {
    setVisible(true)
  }

  const closeModal = () => {
    setVisible(false)
    if (value && JSON.stringify(value) !== JSON.stringify(dataSelected)) {
      setDataSelected(value)
    }
  }

  const handleSubmit = () => {
    setVisible(false)
    onSubmit && onSubmit(dataSelected)
  }

  const handleSelect = (item: Item<ItemT>) => {
    if (dataSelected.find((i) => i?.id === item?.id)) {
      const newDataSelected = dataSelected.filter((i) => i.id !== item.id)
      setDataSelected(newDataSelected)
    } else {
      const newDataSelected = [...dataSelected, item]
      setDataSelected(newDataSelected)
    }
  }

  const keyExtractor = (_: Item<ItemT>, index: number) => index?.toString()

  const renderItem = ({ item }) => {
    const isSelected = Boolean(dataSelected.find((i) => i.id === item.id))
    return (
      <Button style={styles.item} onPress={() => handleSelect(item)}>
        <Text>{getItemLabel ? getItemLabel?.(item) : item?.name}</Text>
        <MCIcons
          size={fontSize.fontSize16}
          name={
            isSelected
              ? "checkbox-marked-circle"
              : "checkbox-blank-circle-outline"
          }
        />
      </Button>
    )
  }

  return (
    <View>
      <Button onPress={openModal}>{children}</Button>
      <Modal
        useNativeDriver
        isVisible={isVisible}
        backdropTransitionOutTiming={0}
        hideModalContentWhileAnimating
        onBackButtonPress={closeModal}
        onBackdropPress={closeModal}
        style={styles.modal}
      >
        <View style={styles.modalContainer}>
          <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={keyExtractor}
            style={styles.content}
            showsVerticalScrollIndicator={false}
            stickyHeaderIndices={[0]}
            ListHeaderComponent={
              <View style={styles.header}>
                {titleModal ? (
                  <Text style={styles.title}>{titleModal}</Text>
                ) : (
                  <View />
                )}
                <Button onPress={handleSubmit}>
                  <Text style={styles.textDone}>Done</Text>
                </Button>
              </View>
            }
          />
          <Button onPress={closeModal} style={styles.btnCancel}>
            <Text>Cancel</Text>
          </Button>
        </View>
      </Modal>
    </View>
  )
}

const styles = StyleSheet.create({
  modal: {
    marginVertical: scale(10),
    marginHorizontal: scale(20),
    justifyContent: "flex-end",
  },
  modalContainer: {
    maxHeight: "90%",
  },
  content: {
    backgroundColor: colors.palette.neutral100,
    borderRadius: scale(12),
  },
  item: {
    paddingVertical: scale(6),
    paddingHorizontal: scale(12),
    borderTopColor: "#F2F2F2",
    borderTopWidth: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  btnCancel: {
    backgroundColor: colors.palette.neutral100,
    borderRadius: scale(12),
    paddingVertical: scale(6),
    marginTop: scale(8),
    alignItems: "center",
  },
  header: {
    backgroundColor: colors.palette.neutral100,
    paddingTop: scale(6),
    paddingBottom: scale(4),
    paddingHorizontal: scale(12),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  title: {
    fontWeight: "700",
  },
  textDone: {
    fontWeight: "700",
  },
})

export { MultiSelect }
export type { IMultiSelectProps }
