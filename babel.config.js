module.exports = {
  presets: ["module:metro-react-native-babel-preset"],
  plugins: [
    [
      "module-resolver",
      {
        extensions: [".ios.js", ".android.js", ".js", ".ts", ".tsx", ".json"],
        alias: {
          "@components": "./src/components",
          "@config": "./src/config",
          "@hooks": "./src/hooks",
          "@i18n": "./src/i18n",
          "@models": "./src/models",
          "@navigators": "./src/navigators",
          "@screens": "./src/screens",
          "@services": "./src/services",
          "@theme": "./src/theme",
          "@utils": "./src/utils",
        },
      },
    ],
  ],
}
