import mockRNDeviceInfo from "react-native-device-info/jest/react-native-device-info-mock"
import mockFbsdk from "react-native-fbsdk-next/jest/mocks"
import mockAsyncStorage from "@react-native-async-storage/async-storage/jest/async-storage-mock"
import mockRNSafeAreaContext from "react-native-safe-area-context/jest/mock"

jest.mock("react-native-config", () => ({
  API_URL: "http://charmsta.softyn.com",
}))

jest.mock("react-native-fbsdk-next", () => {
  return {
    ...mockFbsdk,
    LoginManager: {
      setLoginBehavior: jest.fn(),
      logInWithPermissions: jest.fn(),
      logOut: jest.fn(),
    },
  }
})

jest.mock(
  "@react-native-async-storage/async-storage",
  () =>
    require("@react-native-async-storage/async-storage/jest/async-storage-mock")
      .default,
)

jest.mock("react-native-device-info", () => mockRNDeviceInfo)

jest.mock(
  "react-native-safe-area-context",
  () => require("react-native-safe-area-context/jest/mock").default,
)
